﻿namespace com_emu_sharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_trace = new System.Windows.Forms.Label();
            this.label_com1 = new System.Windows.Forms.Label();
            this.combo_com_n1 = new System.Windows.Forms.ComboBox();
            this.button_finish_n1 = new System.Windows.Forms.Button();
            this.button_start_n1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.combo_com_n2 = new System.Windows.Forms.ComboBox();
            this.button_finish_n2 = new System.Windows.Forms.Button();
            this.button_start_n2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_trace
            // 
            this.label_trace.AutoSize = true;
            this.label_trace.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_trace.Location = new System.Drawing.Point(55, 19);
            this.label_trace.Name = "label_trace";
            this.label_trace.Size = new System.Drawing.Size(117, 24);
            this.label_trace.TabIndex = 1;
            this.label_trace.Text = "Трасса №1";
            // 
            // label_com1
            // 
            this.label_com1.AutoSize = true;
            this.label_com1.Location = new System.Drawing.Point(17, 61);
            this.label_com1.Name = "label_com1";
            this.label_com1.Size = new System.Drawing.Size(60, 13);
            this.label_com1.TabIndex = 6;
            this.label_com1.Text = "COM-порт:";
            // 
            // combo_com_n1
            // 
            this.combo_com_n1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_com_n1.FormattingEnabled = true;
            this.combo_com_n1.Location = new System.Drawing.Point(81, 58);
            this.combo_com_n1.Name = "combo_com_n1";
            this.combo_com_n1.Size = new System.Drawing.Size(121, 21);
            this.combo_com_n1.TabIndex = 5;
            this.combo_com_n1.SelectedIndexChanged += new System.EventHandler(this.combo_com_n1_SelectedIndexChanged);
            // 
            // button_finish_n1
            // 
            this.button_finish_n1.Location = new System.Drawing.Point(131, 96);
            this.button_finish_n1.Name = "button_finish_n1";
            this.button_finish_n1.Size = new System.Drawing.Size(71, 74);
            this.button_finish_n1.TabIndex = 3;
            this.button_finish_n1.Text = "Финиш";
            this.button_finish_n1.UseVisualStyleBackColor = true;
            this.button_finish_n1.Click += new System.EventHandler(this.button_finish_n1_Click);
            // 
            // button_start_n1
            // 
            this.button_start_n1.Location = new System.Drawing.Point(20, 96);
            this.button_start_n1.Name = "button_start_n1";
            this.button_start_n1.Size = new System.Drawing.Size(71, 74);
            this.button_start_n1.TabIndex = 4;
            this.button_start_n1.Text = "Старт";
            this.button_start_n1.UseVisualStyleBackColor = true;
            this.button_start_n1.Click += new System.EventHandler(this.button_start_n1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(276, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "COM-порт:";
            // 
            // combo_com_n2
            // 
            this.combo_com_n2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_com_n2.FormattingEnabled = true;
            this.combo_com_n2.Location = new System.Drawing.Point(340, 58);
            this.combo_com_n2.Name = "combo_com_n2";
            this.combo_com_n2.Size = new System.Drawing.Size(121, 21);
            this.combo_com_n2.TabIndex = 10;
            this.combo_com_n2.SelectedIndexChanged += new System.EventHandler(this.combo_com_n2_SelectedIndexChanged);
            // 
            // button_finish_n2
            // 
            this.button_finish_n2.Location = new System.Drawing.Point(390, 96);
            this.button_finish_n2.Name = "button_finish_n2";
            this.button_finish_n2.Size = new System.Drawing.Size(71, 74);
            this.button_finish_n2.TabIndex = 8;
            this.button_finish_n2.Text = "Финиш";
            this.button_finish_n2.UseVisualStyleBackColor = true;
            this.button_finish_n2.Click += new System.EventHandler(this.button_finish_n2_Click);
            // 
            // button_start_n2
            // 
            this.button_start_n2.Location = new System.Drawing.Point(279, 96);
            this.button_start_n2.Name = "button_start_n2";
            this.button_start_n2.Size = new System.Drawing.Size(71, 74);
            this.button_start_n2.TabIndex = 9;
            this.button_start_n2.Text = "Старт";
            this.button_start_n2.UseVisualStyleBackColor = true;
            this.button_start_n2.Click += new System.EventHandler(this.button_start_n2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(314, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "Трасса №2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 187);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.combo_com_n2);
            this.Controls.Add(this.button_finish_n2);
            this.Controls.Add(this.button_start_n2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_com1);
            this.Controls.Add(this.combo_com_n1);
            this.Controls.Add(this.button_finish_n1);
            this.Controls.Add(this.button_start_n1);
            this.Controls.Add(this.label_trace);
            this.Name = "Form1";
            this.Text = "Эмулятор датчиков";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_trace;
        private System.Windows.Forms.Label label_com1;
        private System.Windows.Forms.ComboBox combo_com_n1;
        private System.Windows.Forms.Button button_finish_n1;
        private System.Windows.Forms.Button button_start_n1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox combo_com_n2;
        private System.Windows.Forms.Button button_finish_n2;
        private System.Windows.Forms.Button button_start_n2;
        private System.Windows.Forms.Label label2;
    }
}

