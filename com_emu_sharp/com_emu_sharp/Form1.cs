﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;
using System.Threading;

namespace com_emu_sharp
{
    public partial class Form1 : Form
    {
        private String[] com_port_names = SerialPort.GetPortNames();
        static SerialPort com_n1;
        static SerialPort com_n2;

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < com_port_names.Length; i++)
                combo_com_n1.Items.Add(com_port_names[i]);
            for (int i = 0; i < com_port_names.Length; i++)
                combo_com_n2.Items.Add(com_port_names[i]);

            //combo_com_n1.SelectedIndex = 0;
            //combo_com_n2.SelectedIndex = 0;

            button_start_n1.BackColor = SystemColors.InactiveBorder;
            button_finish_n1.BackColor = SystemColors.InactiveBorder;
            button_start_n2.BackColor = SystemColors.InactiveBorder;
            button_finish_n2.BackColor = SystemColors.InactiveBorder;
        }

        private void combo_com_n1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Закрываем порт, если он открыт
            com_n1 = COM_Close(com_n1);

            // Открываем новый порт
            com_n1 = COM_Open(com_n1, combo_com_n1.Text, button_start_n1, button_finish_n1);
        }

        private void combo_com_n2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Закрываем порт, если он открыт
            com_n2 = COM_Close(com_n2);

            // Открываем новый порт
            com_n2 = COM_Open(com_n2, combo_com_n2.Text, button_start_n2, button_finish_n2);

        }


        // ------------------------------------------------------------------------
        // Открытие порта
        private SerialPort COM_Open(SerialPort com_port, String com_name, Button bt_start, Button bt_finish)
        {
            if (com_port == null)
            {
                com_port = new SerialPort(com_name);
                com_port.Handshake = Handshake.XOnXOff;
                com_port.DtrEnable = false;
                com_port.RtsEnable = false;
                bt_start.BackColor = SystemColors.InactiveBorder;
                bt_finish.BackColor = SystemColors.InactiveBorder;
                try
                {
                    com_port.Open();
                }
                catch (System.UnauthorizedAccessException)
                {
                    com_port = null;
                    MessageBox.Show("COM-порт занят");
                }
                catch (System.IO.IOException)
                {
                    com_port = null;
                    MessageBox.Show("Не удается открыть COM-порт");
                }
            
            }

            return com_port;
        }

        // ------------------------------------------------------------------------
        // Закрытие порта
        private SerialPort COM_Close(SerialPort com_port)
        {
            if (com_port != null)
            {
                if (com_port.IsOpen)
                    com_port.Close();
                com_port = null;
            }
            return com_port;
        }


        // ------------------------------------------------------------------------
        // Закрытие окна
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            com_n1 = COM_Close(com_n1);
            com_n2 = COM_Close(com_n2);
        }

        // ------------------------------------------------------------------------
        // Кнопки
        private void button_start_n1_Click(object sender, EventArgs e)
        {
            Click_Start(com_n1, button_start_n1);
        }

        private void button_finish_n1_Click(object sender, EventArgs e)
        {
            Click_Finish(com_n1, button_finish_n1);
        }

        private void button_start_n2_Click(object sender, EventArgs e)
        {
            Click_Start(com_n2, button_start_n2);
        }

        private void button_finish_n2_Click(object sender, EventArgs e)
        {
            Click_Finish(com_n2, button_finish_n2);
        }

        private void Click_Start(SerialPort com_port, Button bt_start)
        {
            if (com_port == null)
            {
                MessageBox.Show("COM-порт занят. Выберите другой COM-порт");
            }
            else
            {
                if (com_port.DtrEnable == false)
                {
                    com_port.DtrEnable = true;
                    bt_start.BackColor = SystemColors.HotTrack;
                }
                else
                {
                    com_port.DtrEnable = false;
                    bt_start.BackColor = SystemColors.InactiveBorder;
                }
            }
        }

        private void Click_Finish(SerialPort com_port, Button bt_finish)
        {
            if (com_port == null)
            {
                MessageBox.Show("COM-порт занят. Выберите другой COM-порт");
            }
            else
            {
                if (com_port.RtsEnable == false)
                {
                    com_port.RtsEnable = true;
                    bt_finish.BackColor = SystemColors.HotTrack;
                }
                else
                {
                    com_port.RtsEnable = false;
                    bt_finish.BackColor = SystemColors.InactiveBorder;
                }
            }
        }
    }
}
