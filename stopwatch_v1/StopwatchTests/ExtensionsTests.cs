﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using stopwatch_v1;

namespace StopwatchTests
{
    class ExtensionsTests
    {
        [Test]
        public void ShouldRoundWhenCeiling()
        {
            TimeSpan timeSpan = new TimeSpan(0, 0, 1, 20, 998);
            string actual = timeSpan.FormatTimeSpan();

            Assert.AreEqual("01:20.99", actual);
        }

        [Test]
        public void ShouldRoundWhenFloor()
        {
            TimeSpan timeSpan = new TimeSpan(0, 0, 1, 20, 0);
            string actual = timeSpan.FormatTimeSpan();

            Assert.AreEqual("01:20.00", actual);
        }

        [Test]
        public void ShouldRound()
        {
            TimeSpan timeSpan = new TimeSpan(0, 0, 1, 20, 337);
            string actual = timeSpan.FormatTimeSpan();

            Assert.AreEqual("01:20.33", actual);
        }

        [Test]
        public void ShouldRound2()
        {
            TimeSpan timeSpan = new TimeSpan(0, 0, 1, 20, 334);
            string actual = timeSpan.FormatTimeSpan();

            Assert.AreEqual("01:20.33", actual);
        }

        [Test]
        public void ShouldReturnTimeSpan()
        {
            TimeSpan actual = "00:01.05".ToTimeSpan();
            TimeSpan expected = new TimeSpan(0, 0, 0, 1, 50);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ShouldManageEmptyString()
        {
            TimeSpan actual = "".ToTimeSpan();
            TimeSpan expected = TimeSpan.Zero;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ShouldConvertBoolToString()
        {
            string actual = true.ToIntString();
            Assert.AreEqual("1", actual);
        }

        [Test]
        public void ShouldConvertIntStringToBool()
        {
            bool actual = "1".ToBool();
            Assert.AreEqual(true, actual);
        }
    }
}
