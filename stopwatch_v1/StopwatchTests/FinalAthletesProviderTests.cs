﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using stopwatch_v1;


namespace StopwatchTests
{
    class FinalAthletesProviderTests
    {
        [Test]
        public void ShouldReturnFirstAthletes()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[0], actual[0]);
            Assert.AreEqual(athletes[1], actual[1]);
        }

        [Test]
        public void ShouldSwapFirstAthletes()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();
            actual[0].ResultT1Cl.ResultTime = GetRandomTimeSpan();
            actual[1].ResultT1Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[0], actual[1]);
            Assert.AreEqual(athletes[1], actual[0]);
        }

        [Test]
        public void ShouldReturnSecondAthletes()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();
            actual[0].ResultT1Cl.ResultTime = GetRandomTimeSpan();
            actual[1].ResultT1Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();
            actual[0].ResultT2Cl.ResultTime = GetRandomTimeSpan();
            actual[1].ResultT2Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[2], actual[0]);
            Assert.AreEqual(athletes[3], actual[1]);
        }

        [Test]
        public void ShouldDisqualifyFirst()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();
            actual[0].ResultT1Cl.Disqualified = true;
            actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[1], actual[1]);
            Assert.IsNull(actual[0]);
        }

        [Test]
        public void ShouldDisqualifySecond()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();
            actual[1].ResultT1Cl.Disqualified = true;
            actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[0], actual[0]);
            Assert.IsNull(actual[1]);
        }

        [Test]
        public void ShouldSwapDisqualifyFirst()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();
            actual[0].ResultT1Cl.Disqualified = true;
            actual = provider.SelectNextAthletes();
            actual[1].ResultT1Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[1], actual[0]);
            Assert.IsNull(actual[1]);
        }

        [Test]
        public void ShouldReturnTwoAfterDisqualifyFirst()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();
            actual[0].ResultT1Cl.Disqualified = true;
            actual = provider.SelectNextAthletes();
            actual[1].ResultT1Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();
            actual[0].ResultT2Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[2], actual[0]);
            Assert.AreEqual(athletes[3], actual[1]);
        }

        [Test]
        public void ShouldReturnTwoAfterDisqualifySecond()
        {
            var athletes = GetTestAthletes();
            var provider = new FinalAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();
            actual[1].ResultT1Cl.Disqualified = true;
            actual = provider.SelectNextAthletes();
            actual[0].ResultT1Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();
            actual[1].ResultT2Cl.ResultTime = GetRandomTimeSpan();
            actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[2], actual[0]);
            Assert.AreEqual(athletes[3], actual[1]);
        }

        private List<Athlete> GetTestAthletes()
        {
            return new List<Athlete>
                       {
                           new Athlete(1),
                           new Athlete(20),
                           new Athlete(13),
                           new Athlete(4),
                       };
        }

        private TimeSpan GetRandomTimeSpan()
        {
            var rnd = new Random();
            return new TimeSpan(0, rnd.Next(2), rnd.Next(60));
        }
    }
}
