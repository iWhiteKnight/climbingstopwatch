﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using stopwatch_v1;

namespace StopwatchTests
{
    class AthleteListSelectorTests
    {
        [Test]
        public void ShouldReturnQuarterFinal()
        {
            List<Athlete> athletes = GetQualificationList();
            var actual = new AthleteListSelector().GetNextRoundAthleteList(athletes, RoundType.Qualification, RoundType.QuarterFinal);

            Assert.AreEqual(athletes[4], actual[0]);
            Assert.AreEqual(athletes[2], actual[1]);
            Assert.AreEqual(athletes[0], actual[2]);
            Assert.AreEqual(athletes[5], actual[3]);
            Assert.AreEqual(athletes[6], actual[4]);
            Assert.AreEqual(athletes[1], actual[5]);
            Assert.AreEqual(athletes[3], actual[6]);
            Assert.AreEqual(athletes[7], actual[7]);
        }


        [Test]
        public void ShouldReturnHalfFinal()
        {
            List<Athlete> athletes = GetQualificationList();
            var actual = new AthleteListSelector().GetNextRoundAthleteList(athletes, RoundType.Qualification, RoundType.HalfFinal);

            Assert.AreEqual(athletes[4], actual[0]);
            Assert.AreEqual(athletes[0], actual[1]);
            Assert.AreEqual(athletes[6], actual[2]);
            Assert.AreEqual(athletes[3], actual[3]);
        }

        [Test]
        public void ShouldReturnHalfFinalAfterQuarter()
        {
            List<Athlete> athletes = GetQuarterFinalList();
            var actual = new AthleteListSelector().GetNextRoundAthleteList(athletes, RoundType.QuarterFinal, RoundType.HalfFinal);

            Assert.AreEqual(athletes[2], actual[0]);
            Assert.AreEqual(athletes[4], actual[1]);
            Assert.AreEqual(athletes[1], actual[2]);
            Assert.AreEqual(athletes[7], actual[3]);
        }

        [Test]
        public void ShouldReturnFinal()
        {
            List<Athlete> athletes = GetPlayOffList();
            var actual = new AthleteListSelector().GetNextRoundAthleteList(athletes, RoundType.HalfFinal, RoundType.Final);

            Assert.AreEqual(athletes[1], actual[0]);
            Assert.AreEqual(athletes[2], actual[1]);
            Assert.AreEqual(athletes[0], actual[2]);
            Assert.AreEqual(athletes[3], actual[3]);
        }
        
        [Test]
        public void ShouldConsiderDisqualifications()
        {
            List<Athlete> athletes = GetQualificationList();
            athletes[4].ResultT1Cl.Disqualified = true;
            var actual = new AthleteListSelector().GetNextRoundAthleteList(athletes, RoundType.Qualification, RoundType.HalfFinal);

            Assert.AreEqual(athletes[6], actual[0]);
            Assert.AreEqual(athletes[5], actual[1]);
            Assert.AreEqual(athletes[3], actual[2]);
            Assert.AreEqual(athletes[0], actual[3]);
        }

        [Test]
        public void ShouldConsiderFailings()
        {
            List<Athlete> athletes = GetQualificationList();
            athletes[4].ResultT1Cl.Fell = true;
            var actual = new AthleteListSelector().GetNextRoundAthleteList(athletes, RoundType.Qualification, RoundType.HalfFinal);

            Assert.AreEqual(athletes[6], actual[0]);
            Assert.AreEqual(athletes[5], actual[1]);
            Assert.AreEqual(athletes[3], actual[2]);
            Assert.AreEqual(athletes[0], actual[3]);
        }

        private List<Athlete> GetQualificationList()
        {
            return new List<Athlete>
                       {
                           new Athlete(1) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 20)}}, //0
                           new Athlete(2) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 29)}}, //1
                           new Athlete(3) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 45)}}, //2
                           new Athlete(4) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 15)}}, //3
                           new Athlete(5) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 2)}}, //4
                           new Athlete(6) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 23)}}, //5
                           new Athlete(7) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 12)}}, //6
                           new Athlete(8) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 25)}}, //7
                           new Athlete(9) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 1, 25)}}, //8
                           
                       };
        }

        private List<Athlete> GetPlayOffList()
        {
            return new List<Athlete>
                       {
                           new Athlete(1) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 4)}},
                           new Athlete(2) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 8)}},
                           new Athlete(3) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 45)}},
                           new Athlete(4) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 15)}},
                       };
        }

        private List<Athlete> GetQuarterFinalList()
        {
            return new List<Athlete>
                       {
                           new Athlete(1) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 9)}}, //0
                           new Athlete(2) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 8)}}, //1  2
                           new Athlete(3) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 5)}}, //2  1
                           new Athlete(4) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 15)}}, //3
                           new Athlete(5) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 21)}}, //4  4
                           new Athlete(6) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 34)}}, //5
                           new Athlete(7) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 23)}}, //6
                           new Athlete(8) {ResultT1Cl = new Result {ResultTime = new TimeSpan(0, 0, 20)}}, //7  3
                       };
        }
    }
}
