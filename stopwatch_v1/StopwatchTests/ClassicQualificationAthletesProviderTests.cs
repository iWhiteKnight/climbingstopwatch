﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using stopwatch_v1;

namespace StopwatchTests
{
    class ClassicQualificationAthletesProviderTests
    {

        [Test]
        public void ShouldReturnFirstSingleAthlete()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            var actual = provider.SelectNextAthletes();

            Assert.AreEqual(actual[0], athletes[0]);
            Assert.IsNull(actual[1]);
        }

        [Test]
        public void ShouldReturnFirstTwoAthletes()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            var actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[1], actual[0]);
            Assert.AreEqual(athletes[0], actual[1]);
        }

        [Test]
        public void ShouldReturnLastOne()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            var actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[4], actual[1]);
            Assert.IsNull(actual[0]);
        }

        [Test]
        public void ShouldReturnNulls()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            var actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[0]);
            Assert.IsNull(actual[1]);
        }

        [Test]
        public void ShouldProcessDisqualifyFirst()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Disqualified = true;
            var actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[1]);
            Assert.AreEqual(athletes[1], actual[0]);
        }

        [Test]
        public void ShouldProcessDisqualify()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Disqualified = true;
            var actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[2], actual[0]);
            Assert.AreEqual(athletes[0], actual[1]);
        }

        [Test]
        public void ShouldProcessDisqualifySecond()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            var current = provider.SelectNextAthletes();
            current[1].ResultT1Cl.Disqualified = true;
            var actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[1]);
            Assert.AreEqual(athletes[1], actual[0]);
        }

        [Test]
        public void ShouldDisqualifyLast()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            var actual = provider.SelectNextAthletes();
            actual[1].ResultT2Cl.Disqualified = true;
            actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[0]);
            Assert.IsNull(actual[1]);
        }

        [Test]
        public void ShouldDisqualifyTwoTimes()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Disqualified = true;
            current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Disqualified = true;
            var actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[3], actual[0]);
            Assert.AreEqual(athletes[0], actual[1]);
        }

        [Test]
        public void ShouldProcessFallFirst()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Fell = true;
            var actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[1]);
            Assert.AreEqual(athletes[1], actual[0]);
        }

        [Test]
        public void ShouldProcessFallFirstTwoTimes()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Fell = true;
            current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Fell = true;
            var actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[1]);
            Assert.AreEqual(athletes[2], actual[0]);
        }

        [Test]
        public void ShouldProcessFallPreLast()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            provider.SelectNextAthletes();
            var actual = provider.SelectNextAthletes();
            actual[0].ResultT1Cl.Fell = true;
            actual[1].ResultT2Cl.ResultTime = new TimeSpan(0, 0, 10);
            actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[0]);
            Assert.IsNull(actual[1]);
        }

        [Test]
        public void ShouldProcessFallFirstCommonCase()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Fell = true;
            var actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[1]);
            Assert.AreEqual(athletes[2], actual[0]);
        }

        [Test]
        public void ShouldProcessFallSecond()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.ResultTime = new TimeSpan(0, 0, 13);
            current[1].ResultT2Cl.Fell = true;
            var actual = provider.SelectNextAthletes();

            Assert.AreEqual(athletes[2], actual[0]);
            Assert.AreEqual(athletes[1], actual[1]);
        }

        [Test]
        public void ShouldProcessFallBoth()
        {
            var athletes = GetTestAthletes();
            var provider = new ClassicQualificationAthletesProvider(athletes);
            provider.SelectNextAthletes();
            var current = provider.SelectNextAthletes();
            current[0].ResultT1Cl.Fell = true;
            current[1].ResultT2Cl.Fell = true;
            var actual = provider.SelectNextAthletes();

            Assert.IsNull(actual[1]);
            Assert.AreEqual(athletes[2], actual[0]);
        }

        private List<Athlete> GetTestAthletes()
        {
            return new List<Athlete>
                       {
                           new Athlete(1),
                           new Athlete(20),
                           new Athlete(13),
                           new Athlete(4),
                           new Athlete(5),
                       };
        }
    }
}
