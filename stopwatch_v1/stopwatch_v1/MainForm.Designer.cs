﻿using System;
namespace stopwatch_v1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.протоколToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attachProtocolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detachProtocolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meny_equipment = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_stage = new System.Windows.Forms.Label();
            this.label_format = new System.Windows.Forms.Label();
            this.button_start = new System.Windows.Forms.Button();
            this.button_stop_n1 = new System.Windows.Forms.Button();
            this.button_stop_n2 = new System.Windows.Forms.Button();
            this.button_fail_n1 = new System.Windows.Forms.Button();
            this.button_fail_n2 = new System.Windows.Forms.Button();
            this.button_trans = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.label_time_n1 = new System.Windows.Forms.Label();
            this.label_time_n2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblProtocol = new System.Windows.Forms.Label();
            this.buttonStartRound = new System.Windows.Forms.Button();
            this.openProtocol = new System.Windows.Forms.OpenFileDialog();
            this.comPortLbl1 = new System.Windows.Forms.Label();
            this.comPortLbl2 = new System.Windows.Forms.Label();
            this.Final = new System.Windows.Forms.TabPage();
            this.dataGridFinal = new System.Windows.Forms.DataGridView();
            this.HalfFinal = new System.Windows.Forms.TabPage();
            this.dataGridHalf = new System.Windows.Forms.DataGridView();
            this.FourthFinal = new System.Windows.Forms.TabPage();
            this.dataGridFour = new System.Windows.Forms.DataGridView();
            this.EigthFinal = new System.Windows.Forms.TabPage();
            this.dataGridEight = new System.Windows.Forms.DataGridView();
            this.Qualification = new System.Windows.Forms.TabPage();
            this.dataGridQual = new System.Windows.Forms.DataGridView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.reorderButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.firstTraceAthlLabel = new System.Windows.Forms.Label();
            this.secTraceAthlLabel = new System.Windows.Forms.Label();
            this.platform1IsNotActive = new System.Windows.Forms.Label();
            this.platform2IsNotActive = new System.Windows.Forms.Label();
            this.upButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.saveXlsFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.Final.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFinal)).BeginInit();
            this.HalfFinal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridHalf)).BeginInit();
            this.FourthFinal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFour)).BeginInit();
            this.EigthFinal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEight)).BeginInit();
            this.Qualification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridQual)).BeginInit();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.протоколToolStripMenuItem,
            this.настройкиToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(754, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // протоколToolStripMenuItem
            // 
            this.протоколToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attachProtocolToolStripMenuItem,
            this.detachProtocolToolStripMenuItem});
            this.протоколToolStripMenuItem.Name = "протоколToolStripMenuItem";
            this.протоколToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.протоколToolStripMenuItem.Text = "Протокол";
            // 
            // attachProtocolToolStripMenuItem
            // 
            this.attachProtocolToolStripMenuItem.Name = "attachProtocolToolStripMenuItem";
            this.attachProtocolToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.attachProtocolToolStripMenuItem.Text = "Подключить протокол";
            this.attachProtocolToolStripMenuItem.Click += new System.EventHandler(this.AttachProtocol_Click);
            // 
            // detachProtocolToolStripMenuItem
            // 
            this.detachProtocolToolStripMenuItem.Name = "detachProtocolToolStripMenuItem";
            this.detachProtocolToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.detachProtocolToolStripMenuItem.Text = "Отключить протокол";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.meny_equipment});
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            // 
            // meny_equipment
            // 
            this.meny_equipment.Name = "meny_equipment";
            this.meny_equipment.Size = new System.Drawing.Size(155, 22);
            this.meny_equipment.Text = "Оборудование";
            this.meny_equipment.Click += new System.EventHandler(this.meny_equipment_settings_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Формат:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Этап:";
            // 
            // label_stage
            // 
            this.label_stage.AutoSize = true;
            this.label_stage.Location = new System.Drawing.Point(214, 60);
            this.label_stage.Name = "label_stage";
            this.label_stage.Size = new System.Drawing.Size(0, 13);
            this.label_stage.TabIndex = 2;
            // 
            // label_format
            // 
            this.label_format.AutoSize = true;
            this.label_format.Location = new System.Drawing.Point(214, 38);
            this.label_format.Name = "label_format";
            this.label_format.Size = new System.Drawing.Size(79, 13);
            this.label_format.TabIndex = 2;
            this.label_format.Text = "Классический";
            // 
            // button_start
            // 
            this.button_start.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button_start.Location = new System.Drawing.Point(309, 108);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(129, 23);
            this.button_start.TabIndex = 3;
            this.button_start.Text = "Старт";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button_stop_n1
            // 
            this.button_stop_n1.Enabled = false;
            this.button_stop_n1.Location = new System.Drawing.Point(292, 152);
            this.button_stop_n1.Name = "button_stop_n1";
            this.button_stop_n1.Size = new System.Drawing.Size(66, 23);
            this.button_stop_n1.TabIndex = 3;
            this.button_stop_n1.Text = "Стоп 1";
            this.button_stop_n1.UseVisualStyleBackColor = true;
            this.button_stop_n1.Click += new System.EventHandler(this.button_stop_n1_Click);
            // 
            // button_stop_n2
            // 
            this.button_stop_n2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_stop_n2.Enabled = false;
            this.button_stop_n2.Location = new System.Drawing.Point(390, 152);
            this.button_stop_n2.Name = "button_stop_n2";
            this.button_stop_n2.Size = new System.Drawing.Size(66, 23);
            this.button_stop_n2.TabIndex = 3;
            this.button_stop_n2.Text = "Стоп 2";
            this.button_stop_n2.UseVisualStyleBackColor = true;
            this.button_stop_n2.Click += new System.EventHandler(this.button_stop_n2_Click);
            // 
            // button_fail_n1
            // 
            this.button_fail_n1.Enabled = false;
            this.button_fail_n1.Location = new System.Drawing.Point(292, 190);
            this.button_fail_n1.Name = "button_fail_n1";
            this.button_fail_n1.Size = new System.Drawing.Size(66, 23);
            this.button_fail_n1.TabIndex = 3;
            this.button_fail_n1.Text = "Срыв 1";
            this.button_fail_n1.UseVisualStyleBackColor = true;
            this.button_fail_n1.Click += new System.EventHandler(this.button_fail_n1_Click);
            // 
            // button_fail_n2
            // 
            this.button_fail_n2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_fail_n2.Enabled = false;
            this.button_fail_n2.Location = new System.Drawing.Point(390, 190);
            this.button_fail_n2.Name = "button_fail_n2";
            this.button_fail_n2.Size = new System.Drawing.Size(66, 23);
            this.button_fail_n2.TabIndex = 3;
            this.button_fail_n2.Text = "Срыв 2";
            this.button_fail_n2.UseVisualStyleBackColor = true;
            this.button_fail_n2.Click += new System.EventHandler(this.button_fail_n2_Click);
            // 
            // button_trans
            // 
            this.button_trans.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button_trans.Enabled = false;
            this.button_trans.Location = new System.Drawing.Point(309, 231);
            this.button_trans.Name = "button_trans";
            this.button_trans.Size = new System.Drawing.Size(129, 23);
            this.button_trans.TabIndex = 3;
            this.button_trans.Text = "Переход";
            this.button_trans.UseVisualStyleBackColor = true;
            this.button_trans.Click += new System.EventHandler(this.button_trans_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.Enabled = false;
            this.button_cancel.Location = new System.Drawing.Point(309, 260);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(129, 23);
            this.button_cancel.TabIndex = 3;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            // 
            // label_time_n1
            // 
            this.label_time_n1.AutoSize = true;
            this.label_time_n1.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_time_n1.Location = new System.Drawing.Point(49, 152);
            this.label_time_n1.Name = "label_time_n1";
            this.label_time_n1.Size = new System.Drawing.Size(179, 43);
            this.label_time_n1.TabIndex = 4;
            this.label_time_n1.Text = "00:00.00";
            // 
            // label_time_n2
            // 
            this.label_time_n2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_time_n2.AutoSize = true;
            this.label_time_n2.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_time_n2.Location = new System.Drawing.Point(516, 152);
            this.label_time_n2.Name = "label_time_n2";
            this.label_time_n2.Size = new System.Drawing.Size(179, 43);
            this.label_time_n2.TabIndex = 4;
            this.label_time_n2.Text = "00:00.00";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(386, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Протокол:";
            // 
            // lblProtocol
            // 
            this.lblProtocol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProtocol.AutoSize = true;
            this.lblProtocol.Location = new System.Drawing.Point(444, 38);
            this.lblProtocol.Name = "lblProtocol";
            this.lblProtocol.Size = new System.Drawing.Size(0, 13);
            this.lblProtocol.TabIndex = 2;
            // 
            // buttonStartRound
            // 
            this.buttonStartRound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStartRound.Enabled = false;
            this.buttonStartRound.Location = new System.Drawing.Point(309, 79);
            this.buttonStartRound.Name = "buttonStartRound";
            this.buttonStartRound.Size = new System.Drawing.Size(129, 23);
            this.buttonStartRound.TabIndex = 7;
            this.buttonStartRound.Text = "Начать этап";
            this.buttonStartRound.UseVisualStyleBackColor = true;
            this.buttonStartRound.Click += new System.EventHandler(this.buttonStartRound_Click);
            // 
            // openProtocol
            // 
            this.openProtocol.FileName = "*.xls";
            this.openProtocol.Filter = "Excel files|*.xls|All files|*.* ";
            this.openProtocol.FilterIndex = 0;
            this.openProtocol.RestoreDirectory = true;
            // 
            // comPortLbl1
            // 
            this.comPortLbl1.AutoSize = true;
            this.comPortLbl1.Location = new System.Drawing.Point(54, 251);
            this.comPortLbl1.Name = "comPortLbl1";
            this.comPortLbl1.Size = new System.Drawing.Size(115, 13);
            this.comPortLbl1.TabIndex = 8;
            this.comPortLbl1.Text = "Не выбран COM порт";
            // 
            // comPortLbl2
            // 
            this.comPortLbl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comPortLbl2.AutoSize = true;
            this.comPortLbl2.Location = new System.Drawing.Point(524, 251);
            this.comPortLbl2.Name = "comPortLbl2";
            this.comPortLbl2.Size = new System.Drawing.Size(115, 13);
            this.comPortLbl2.TabIndex = 9;
            this.comPortLbl2.Text = "Не выбран COM порт";
            // 
            // Final
            // 
            this.Final.Controls.Add(this.dataGridFinal);
            this.Final.Location = new System.Drawing.Point(4, 22);
            this.Final.Name = "Final";
            this.Final.Padding = new System.Windows.Forms.Padding(3);
            this.Final.Size = new System.Drawing.Size(664, 178);
            this.Final.TabIndex = 4;
            this.Final.Text = "Финал";
            this.Final.UseVisualStyleBackColor = true;
            // 
            // dataGridFinal
            // 
            this.dataGridFinal.AllowUserToAddRows = false;
            this.dataGridFinal.AllowUserToResizeRows = false;
            this.dataGridFinal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridFinal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridFinal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridFinal.Location = new System.Drawing.Point(3, 3);
            this.dataGridFinal.Name = "dataGridFinal";
            this.dataGridFinal.Size = new System.Drawing.Size(658, 172);
            this.dataGridFinal.TabIndex = 0;
            // 
            // HalfFinal
            // 
            this.HalfFinal.Controls.Add(this.dataGridHalf);
            this.HalfFinal.Location = new System.Drawing.Point(4, 22);
            this.HalfFinal.Name = "HalfFinal";
            this.HalfFinal.Padding = new System.Windows.Forms.Padding(3);
            this.HalfFinal.Size = new System.Drawing.Size(664, 178);
            this.HalfFinal.TabIndex = 3;
            this.HalfFinal.Text = "Полуфинал";
            this.HalfFinal.UseVisualStyleBackColor = true;
            // 
            // dataGridHalf
            // 
            this.dataGridHalf.AllowUserToAddRows = false;
            this.dataGridHalf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridHalf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridHalf.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridHalf.Location = new System.Drawing.Point(3, 3);
            this.dataGridHalf.Name = "dataGridHalf";
            this.dataGridHalf.Size = new System.Drawing.Size(658, 172);
            this.dataGridHalf.TabIndex = 0;
            // 
            // FourthFinal
            // 
            this.FourthFinal.Controls.Add(this.dataGridFour);
            this.FourthFinal.Location = new System.Drawing.Point(4, 22);
            this.FourthFinal.Name = "FourthFinal";
            this.FourthFinal.Padding = new System.Windows.Forms.Padding(3);
            this.FourthFinal.Size = new System.Drawing.Size(664, 178);
            this.FourthFinal.TabIndex = 2;
            this.FourthFinal.Text = "1/4 финала";
            this.FourthFinal.UseVisualStyleBackColor = true;
            // 
            // dataGridFour
            // 
            this.dataGridFour.AllowUserToAddRows = false;
            this.dataGridFour.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridFour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridFour.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridFour.Location = new System.Drawing.Point(3, 3);
            this.dataGridFour.Name = "dataGridFour";
            this.dataGridFour.Size = new System.Drawing.Size(658, 172);
            this.dataGridFour.TabIndex = 0;
            // 
            // EigthFinal
            // 
            this.EigthFinal.Controls.Add(this.dataGridEight);
            this.EigthFinal.Location = new System.Drawing.Point(4, 22);
            this.EigthFinal.Name = "EigthFinal";
            this.EigthFinal.Padding = new System.Windows.Forms.Padding(3);
            this.EigthFinal.Size = new System.Drawing.Size(664, 178);
            this.EigthFinal.TabIndex = 1;
            this.EigthFinal.Text = "1/8 финала";
            this.EigthFinal.UseVisualStyleBackColor = true;
            // 
            // dataGridEight
            // 
            this.dataGridEight.AllowUserToAddRows = false;
            this.dataGridEight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridEight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridEight.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridEight.Location = new System.Drawing.Point(3, 3);
            this.dataGridEight.Name = "dataGridEight";
            this.dataGridEight.Size = new System.Drawing.Size(658, 172);
            this.dataGridEight.TabIndex = 0;
            // 
            // Qualification
            // 
            this.Qualification.AutoScroll = true;
            this.Qualification.Controls.Add(this.dataGridQual);
            this.Qualification.Location = new System.Drawing.Point(4, 22);
            this.Qualification.Name = "Qualification";
            this.Qualification.Padding = new System.Windows.Forms.Padding(3);
            this.Qualification.Size = new System.Drawing.Size(664, 178);
            this.Qualification.TabIndex = 0;
            this.Qualification.Text = "Квалификация";
            this.Qualification.UseVisualStyleBackColor = true;
            // 
            // dataGridQual
            // 
            this.dataGridQual.AllowUserToAddRows = false;
            this.dataGridQual.AllowUserToResizeRows = false;
            this.dataGridQual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridQual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridQual.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridQual.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dataGridQual.Location = new System.Drawing.Point(3, 3);
            this.dataGridQual.Name = "dataGridQual";
            this.dataGridQual.ReadOnly = true;
            this.dataGridQual.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridQual.Size = new System.Drawing.Size(658, 172);
            this.dataGridQual.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.Qualification);
            this.tabControl.Controls.Add(this.EigthFinal);
            this.tabControl.Controls.Add(this.FourthFinal);
            this.tabControl.Controls.Add(this.HalfFinal);
            this.tabControl.Controls.Add(this.Final);
            this.tabControl.Location = new System.Drawing.Point(57, 302);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(672, 204);
            this.tabControl.TabIndex = 8;
            this.tabControl.Tag = "";
            // 
            // reorderButton
            // 
            this.reorderButton.Location = new System.Drawing.Point(7, 324);
            this.reorderButton.Name = "reorderButton";
            this.reorderButton.Size = new System.Drawing.Size(48, 29);
            this.reorderButton.TabIndex = 3;
            this.reorderButton.Text = "Изм.";
            this.reorderButton.UseVisualStyleBackColor = true;
            this.reorderButton.Click += new System.EventHandler(this.reorderButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(7, 450);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(48, 40);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Сохр.";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // firstTraceAthlLabel
            // 
            this.firstTraceAthlLabel.AutoSize = true;
            this.firstTraceAthlLabel.Location = new System.Drawing.Point(61, 88);
            this.firstTraceAthlLabel.Name = "firstTraceAthlLabel";
            this.firstTraceAthlLabel.Size = new System.Drawing.Size(52, 13);
            this.firstTraceAthlLabel.TabIndex = 10;
            this.firstTraceAthlLabel.Text = "---------------";
            // 
            // secTraceAthlLabel
            // 
            this.secTraceAthlLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.secTraceAthlLabel.AutoSize = true;
            this.secTraceAthlLabel.Location = new System.Drawing.Point(552, 89);
            this.secTraceAthlLabel.Name = "secTraceAthlLabel";
            this.secTraceAthlLabel.Size = new System.Drawing.Size(49, 13);
            this.secTraceAthlLabel.TabIndex = 11;
            this.secTraceAthlLabel.Text = "--------------";
            // 
            // platform1IsNotActive
            // 
            this.platform1IsNotActive.AutoSize = true;
            this.platform1IsNotActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.platform1IsNotActive.ForeColor = System.Drawing.Color.Red;
            this.platform1IsNotActive.Location = new System.Drawing.Point(50, 231);
            this.platform1IsNotActive.Name = "platform1IsNotActive";
            this.platform1IsNotActive.Size = new System.Drawing.Size(30, 39);
            this.platform1IsNotActive.TabIndex = 12;
            this.platform1IsNotActive.Text = "*";
            this.platform1IsNotActive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.platform1IsNotActive.Visible = false;
            // 
            // platform2IsNotActive
            // 
            this.platform2IsNotActive.AutoSize = true;
            this.platform2IsNotActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.platform2IsNotActive.ForeColor = System.Drawing.Color.Red;
            this.platform2IsNotActive.Location = new System.Drawing.Point(517, 231);
            this.platform2IsNotActive.Name = "platform2IsNotActive";
            this.platform2IsNotActive.Size = new System.Drawing.Size(30, 39);
            this.platform2IsNotActive.TabIndex = 12;
            this.platform2IsNotActive.Text = "*";
            this.platform2IsNotActive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.platform2IsNotActive.Visible = false;
            // 
            // upButton
            // 
            this.upButton.Location = new System.Drawing.Point(7, 359);
            this.upButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(48, 23);
            this.upButton.TabIndex = 13;
            this.upButton.Text = "↑";
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // downButton
            // 
            this.downButton.Location = new System.Drawing.Point(7, 382);
            this.downButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(48, 23);
            this.downButton.TabIndex = 14;
            this.downButton.Text = "↓";
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.downButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(7, 411);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(48, 23);
            this.removeButton.TabIndex = 15;
            this.removeButton.Text = "X";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // saveXlsFileDialog
            // 
            this.saveXlsFileDialog.DefaultExt = "xls";
            this.saveXlsFileDialog.FileName = "*.xls";
            this.saveXlsFileDialog.Filter = "Excel files|*.xls|All files|*.*";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 518);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.downButton);
            this.Controls.Add(this.upButton);
            this.Controls.Add(this.secTraceAthlLabel);
            this.Controls.Add(this.firstTraceAthlLabel);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.reorderButton);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.comPortLbl2);
            this.Controls.Add(this.comPortLbl1);
            this.Controls.Add(this.buttonStartRound);
            this.Controls.Add(this.label_time_n2);
            this.Controls.Add(this.label_time_n1);
            this.Controls.Add(this.button_stop_n2);
            this.Controls.Add(this.button_fail_n2);
            this.Controls.Add(this.button_fail_n1);
            this.Controls.Add(this.button_stop_n1);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_trans);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.label_stage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblProtocol);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label_format);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.platform1IsNotActive);
            this.Controls.Add(this.platform2IsNotActive);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(762, 545);
            this.Name = "MainForm";
            this.Text = "Секундомер";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form_main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.Final.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFinal)).EndInit();
            this.HalfFinal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridHalf)).EndInit();
            this.FourthFinal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFour)).EndInit();
            this.EigthFinal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEight)).EndInit();
            this.Qualification.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridQual)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem протоколToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_stage;
        private System.Windows.Forms.Label label_format;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblProtocol;
        public System.Windows.Forms.Label label_time_n1;
        public System.Windows.Forms.Label label_time_n2;
        private System.Windows.Forms.Label comPortLbl1;
        private System.Windows.Forms.Label comPortLbl2;
        private System.Windows.Forms.OpenFileDialog openProtocol;
        private System.Windows.Forms.TabPage Final;
        private System.Windows.Forms.DataGridView dataGridFinal;
        private System.Windows.Forms.TabPage HalfFinal;
        private System.Windows.Forms.DataGridView dataGridHalf;
        private System.Windows.Forms.TabPage FourthFinal;
        private System.Windows.Forms.DataGridView dataGridFour;
        private System.Windows.Forms.TabPage EigthFinal;
        private System.Windows.Forms.DataGridView dataGridEight;
        private System.Windows.Forms.TabPage Qualification;
        private System.Windows.Forms.DataGridView dataGridQual;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Label firstTraceAthlLabel;
        private System.Windows.Forms.Label secTraceAthlLabel;
        private System.Windows.Forms.Label platform1IsNotActive;
        private System.Windows.Forms.Label platform2IsNotActive;
        internal System.Windows.Forms.Button buttonStartRound;
        internal System.Windows.Forms.ToolStripMenuItem attachProtocolToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem detachProtocolToolStripMenuItem;
        internal System.Windows.Forms.Button button_start;
        internal System.Windows.Forms.Button button_stop_n1;
        internal System.Windows.Forms.Button button_stop_n2;
        internal System.Windows.Forms.Button button_fail_n1;
        internal System.Windows.Forms.Button button_fail_n2;
        internal System.Windows.Forms.Button button_trans;
        internal System.Windows.Forms.Button button_cancel;
        internal System.Windows.Forms.ToolStripMenuItem meny_equipment;
        internal System.Windows.Forms.Button reorderButton;
        internal System.Windows.Forms.Button saveButton;
        internal System.Windows.Forms.Button upButton;
        internal System.Windows.Forms.Button downButton;
        internal System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.SaveFileDialog saveXlsFileDialog;

    }
}

