﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    abstract class AthletesProviderBase : IAthletesProvider
    {
        protected IList<AthleteRun> _list;

        protected AthletesProviderBase(IList<Athlete> list)
        {
            _list = list.Select(item => new AthleteRun(item)).ToList();
        }

        public abstract Athlete[] SelectNextAthletes();
        public abstract void FalseStart(Athlete athlete);
        public abstract bool ListEnded { get; set; }

        protected class AthleteRun
        {
            public Athlete Athlete{ get; set; }
            public int RunClunt { get; set; }

            public AthleteRun(Athlete athlete)
            {
                Athlete = athlete;
            }
        }
    }
}
