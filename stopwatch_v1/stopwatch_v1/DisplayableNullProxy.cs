﻿namespace stopwatch_v1
{
    class DisplayableNullProxy : IDisplayable
    {
        private readonly IDisplayable _displayable;

        public DisplayableNullProxy(IDisplayable displayable)
        {
            _displayable = displayable;
        }

        public void Display(string text)
        {
            if (_displayable != null)
            {
                _displayable.Display(text);
            }
        }
    }
}
