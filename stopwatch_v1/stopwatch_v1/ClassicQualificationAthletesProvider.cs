﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    public class ClassicQualificationAthletesProvider : IAthletesProvider
    {
        private IList<Athlete> _list;
        private Athlete _firstAthlete;
        private Athlete _secAthlete;


        public ClassicQualificationAthletesProvider(IList<Athlete> list)
        {
            _list = list;
        }

        public Athlete[] SelectNextAthletes()
        {
            Athlete[] athlArray = new Athlete[2];

            if (_list.Count > 1)
            {
                if (_firstAthlete == null && _secAthlete == null) // beginning of round
                {
                    athlArray[0] = _list[0];
                }
                else if (_firstAthlete != null && _secAthlete == null) //second race or _second was disqualified
                {
                    var i = _list.IndexOf(_firstAthlete);

                    if (CanContinueCompetition(_firstAthlete)) //first move to trace 2
                    {
                        athlArray[1] = _firstAthlete;
                    }
                    athlArray[0] = GetNext(i);
                }

                else if (_firstAthlete == null && _secAthlete != null) //last race
                {
                }
                else // common case 
                {
                    var i = _list.IndexOf(_firstAthlete);

                    if (CanContinueCompetition(_firstAthlete) && CanContinueCompetition(_secAthlete))
                    {
                        athlArray[0] = GetNext(i);

                        athlArray[1] = _firstAthlete;
                    }
                    else if (CanContinueCompetition(_firstAthlete) && !CanContinueCompetition(_secAthlete))//second disqualified -> he is thrown away first starts again
                    {
                        if (_secAthlete.IsDisqualified)
                        {
                            athlArray[0] = _firstAthlete;
                        }
                        else if (_secAthlete.IsFallen) //make transition because athletes ran
                        {
                            athlArray[0] = GetNext(i);
                            athlArray[1] = _firstAthlete;
                        }
                    }
                    else if (!CanContinueCompetition(_firstAthlete) && CanContinueCompetition(_secAthlete))
                    {
                        if (_firstAthlete.IsDisqualified)
                            //first disqualified -> he is replaced by next (if exists) second starts again
                        {
                            athlArray[1] = _secAthlete;

                            athlArray[0] = GetNext(i);
                        }
                        else //first is fallen -> second has run -> both should be replaced
                        {
                            athlArray[0] = GetNext(i);
                        }
                    }
                    else //Cannot run both
                    {
                        athlArray[0] = GetNext(i);
                    }
                }
                _firstAthlete = athlArray[0];
                _secAthlete = athlArray[1];

                return athlArray;
            }
            else throw new InvalidOperationException("Лист спортсменов пустой");
        }

        public void SetLastActivePair(Athlete firstAthlete, Athlete secondAthlete)
        {
            _firstAthlete = firstAthlete;
            _secAthlete = secondAthlete;
        }

        private bool CanContinueCompetition(Athlete athlete)
        {
            return !athlete.IsDisqualified && !athlete.IsFallen;
        }

        private Athlete GetNext(int i)
        {
            if (i + 1 < _list.Count)// first is not last in list -> next move to his place 
            {
                return _list[i + 1];
            }
            return null;
        }
    }
}
