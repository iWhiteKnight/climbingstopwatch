﻿namespace stopwatch_v1
{
    public enum RoundType
    {
        //Actual values are significant
        Undefined = 0,
        Qualification = 1,
        OneEight = 2,
        QuarterFinal = 3,
        HalfFinal = 4,
        Final = 5
    }
}
