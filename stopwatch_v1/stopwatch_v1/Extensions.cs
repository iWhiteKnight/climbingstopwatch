﻿using System;
using System.Globalization;

namespace stopwatch_v1
{
    public static class Extensions
    {
        private const string TIME_SPAN_FORMAT = @"mm\:ss\.ff";

        public static string FormatTimeSpan(this TimeSpan timeSpan)
        {
            return timeSpan.ToString(TIME_SPAN_FORMAT);
        }

        public static TimeSpan ToTimeSpan(this string tspanString)
        {
            if (String.IsNullOrEmpty(tspanString))
            {
                return TimeSpan.Zero;
            }
            return TimeSpan.ParseExact(tspanString, TIME_SPAN_FORMAT, CultureInfo.CurrentCulture);
        }

        public static string GetString(this RoundType roundType)
        {
            switch (roundType)
            {
                case RoundType.Qualification:
                    return "Квалификация";
                case RoundType.OneEight:
                    return "Одна восьмая";
                case RoundType.QuarterFinal:
                    return "Четвертьфинал";
                case RoundType.HalfFinal:
                    return "Полуфинал";
                case RoundType.Final:
                    return "Финал";
                default:
                    return "Неизвестно";
            }
        }

        public static string ToIntString(this bool value)
        {
            return Convert.ToInt32(value).ToString(CultureInfo.InvariantCulture);
        }

        public static bool ToBool(this string value)
        {
            return Convert.ToBoolean(Int32.Parse(value));
        }
    }
}
