﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    public class Athlete : IComparable<Athlete>, ICloneable
    {
        public Athlete(int id)
        {
            AthleteID = id;
            ResultT1Cl = new Result { ResultTime = new TimeSpan() };
            ResultT2Cl = new Result { ResultTime = new TimeSpan() };
        }

        public int AthleteID { get; protected set; }

        public string FIO { get; set; }

        public Result ResultT1Cl { get; set; }

        public Result ResultT2Cl { get; set; }

        public TimeSpan ResultTSumClassic
        {
            get
            {
                return ResultT1Cl.ResultTime + ResultT2Cl.ResultTime;
            }
        }

        public string ResultSumString
        {
            get
            {
                return ResultTSumClassic.FormatTimeSpan();
            }
        }

        public bool IsWaiting
        {
            get
            {
                return !FinishedRound && !IsDisqualified;
            }
        }

        public bool HasFalsStart
        {
            get
            {
                return ResultT1Cl.FalseStart || ResultT2Cl.FalseStart;
            }
        } 

        public bool IsDisqualified
        {
            get
            {
                return ResultT1Cl.Disqualified || ResultT2Cl.Disqualified || ExplicitlyDisqualified;
            }
        }

        public bool ExplicitlyDisqualified { get; set; }

        public bool IsFallen
        {
            get
            {
                return ResultT1Cl.Fell || ResultT2Cl.Fell;
            }
        }

        public bool HasResult
        {
            get
            {
                return ResultTSumClassic > TimeSpan.Zero ||
                       IsDisqualified ||
                       IsFallen ||
                       HasFalsStart;
            }
        }

        public bool FinishedRound
        {
            get
            {
                return ((Time1 > TimeSpan.Zero || ResultT1Cl.Fell) && (Time2 > TimeSpan.Zero || ResultT2Cl.Fell));
            }
        }

        public bool WasSelected { get; set; }

        public bool CanBeSelected
        {
            get { return !WasSelected && !IsDisqualified; }
        }

        private TimeSpan Time1
        {
            get
            {
                return ResultT1Cl.ResultTime;
            }
        }

        private TimeSpan Time2
        {
            get
            {
                return ResultT2Cl.ResultTime;
            }
        }

        private int FallCount
        {
            get
            {
                int count = 0;
                if (ResultT1Cl.Fell)
                {
                    count++;
                }
                if (ResultT2Cl.Fell)
                {
                    count++;
                }
                return count;
            }
        }




        public int CompareTo(Athlete other)
        {
            if (IsDisqualified && other.IsDisqualified)
            {
                return 0;
            }
            if (!IsDisqualified && other.IsDisqualified)
            {
                return -1;
            }
            if (IsDisqualified && !other.IsDisqualified)
            {
                return 1;
            }

            if (FallCount < other.FallCount)
            {
                return -1;
            }
            if (FallCount > other.FallCount)
            {
                return 1;
            }

            if (ResultTSumClassic < other.ResultTSumClassic)
            {
                return -1;
            }
            if (ResultTSumClassic > other.ResultTSumClassic)
            {
                return 1;
            }
            return 0;
        }

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        public override string ToString()
        {
            return String.Format("Athlete #{0}", AthleteID);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Athlete))
            {
                return false;
            }
            return ((Athlete) obj).AthleteID == AthleteID;
        }

        public override int GetHashCode()
        {
            return AthleteID.GetHashCode();
        }
    }
}
