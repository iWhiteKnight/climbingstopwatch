﻿namespace stopwatch_v1
{
    partial class Form_test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_trace = new System.Windows.Forms.Label();
            this.button_start = new System.Windows.Forms.Button();
            this.button_end = new System.Windows.Forms.Button();
            this.button_finish = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_trace
            // 
            this.label_trace.AutoSize = true;
            this.label_trace.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_trace.Location = new System.Drawing.Point(87, 29);
            this.label_trace.Name = "label_trace";
            this.label_trace.Size = new System.Drawing.Size(106, 24);
            this.label_trace.TabIndex = 0;
            this.label_trace.Text = "Трасса №";
            // 
            // button_start
            // 
            this.button_start.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.button_start.Enabled = false;
            this.button_start.Location = new System.Drawing.Point(30, 82);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(75, 71);
            this.button_start.TabIndex = 1;
            this.button_start.Text = "Старт";
            this.button_start.UseVisualStyleBackColor = false;
            // 
            // button_end
            // 
            this.button_end.Location = new System.Drawing.Point(91, 180);
            this.button_end.Name = "button_end";
            this.button_end.Size = new System.Drawing.Size(102, 23);
            this.button_end.TabIndex = 2;
            this.button_end.Text = "Завершить";
            this.button_end.UseVisualStyleBackColor = true;
            this.button_end.Click += new System.EventHandler(this.button_end_Click);
            // 
            // button_finish
            // 
            this.button_finish.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.button_finish.Enabled = false;
            this.button_finish.Location = new System.Drawing.Point(165, 82);
            this.button_finish.Name = "button_finish";
            this.button_finish.Size = new System.Drawing.Size(75, 71);
            this.button_finish.TabIndex = 1;
            this.button_finish.Text = "Финиш";
            this.button_finish.UseVisualStyleBackColor = false;
            // 
            // Form_test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 222);
            this.Controls.Add(this.button_end);
            this.Controls.Add(this.button_finish);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.label_trace);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_test";
            this.Text = "Тестирование";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_test_closing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_trace;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_end;
        private System.Windows.Forms.Button button_finish;
    }
}