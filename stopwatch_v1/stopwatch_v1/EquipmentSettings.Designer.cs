﻿namespace stopwatch_v1
{
    partial class EquipmentSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_test_n1 = new System.Windows.Forms.Button();
            this.label_com_n1 = new System.Windows.Forms.Label();
            this.combo_com_n1 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_test_n2 = new System.Windows.Forms.Button();
            this.label_com_n2 = new System.Windows.Forms.Label();
            this.combo_com_n2 = new System.Windows.Forms.ComboBox();
            this.button_accept = new System.Windows.Forms.Button();
            this.button_discard = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_test_n1);
            this.groupBox1.Controls.Add(this.label_com_n1);
            this.groupBox1.Controls.Add(this.combo_com_n1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 115);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Трасса №1";
            // 
            // button_test_n1
            // 
            this.button_test_n1.Location = new System.Drawing.Point(69, 74);
            this.button_test_n1.Name = "button_test_n1";
            this.button_test_n1.Size = new System.Drawing.Size(75, 23);
            this.button_test_n1.TabIndex = 2;
            this.button_test_n1.Text = "Тест";
            this.button_test_n1.UseVisualStyleBackColor = true;
            this.button_test_n1.Click += new System.EventHandler(this.button_test_n1_Click);
            // 
            // label_com_n1
            // 
            this.label_com_n1.AutoSize = true;
            this.label_com_n1.Location = new System.Drawing.Point(6, 30);
            this.label_com_n1.Name = "label_com_n1";
            this.label_com_n1.Size = new System.Drawing.Size(57, 13);
            this.label_com_n1.TabIndex = 1;
            this.label_com_n1.Text = "COM-порт";
            // 
            // combo_com_n1
            // 
            this.combo_com_n1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_com_n1.FormattingEnabled = true;
            this.combo_com_n1.Location = new System.Drawing.Point(69, 27);
            this.combo_com_n1.Name = "combo_com_n1";
            this.combo_com_n1.Size = new System.Drawing.Size(121, 21);
            this.combo_com_n1.TabIndex = 0;
            this.combo_com_n1.SelectedIndexChanged += new System.EventHandler(this.combo_com_n1_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_test_n2);
            this.groupBox2.Controls.Add(this.label_com_n2);
            this.groupBox2.Controls.Add(this.combo_com_n2);
            this.groupBox2.Location = new System.Drawing.Point(230, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(202, 115);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Трасса №2";
            // 
            // button_test_n2
            // 
            this.button_test_n2.Location = new System.Drawing.Point(69, 74);
            this.button_test_n2.Name = "button_test_n2";
            this.button_test_n2.Size = new System.Drawing.Size(75, 23);
            this.button_test_n2.TabIndex = 2;
            this.button_test_n2.Text = "Тест";
            this.button_test_n2.UseVisualStyleBackColor = true;
            this.button_test_n2.Click += new System.EventHandler(this.button_test_n2_Click);
            // 
            // label_com_n2
            // 
            this.label_com_n2.AutoSize = true;
            this.label_com_n2.Location = new System.Drawing.Point(6, 30);
            this.label_com_n2.Name = "label_com_n2";
            this.label_com_n2.Size = new System.Drawing.Size(57, 13);
            this.label_com_n2.TabIndex = 1;
            this.label_com_n2.Text = "COM-порт";
            // 
            // combo_com_n2
            // 
            this.combo_com_n2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_com_n2.FormattingEnabled = true;
            this.combo_com_n2.Location = new System.Drawing.Point(69, 27);
            this.combo_com_n2.Name = "combo_com_n2";
            this.combo_com_n2.Size = new System.Drawing.Size(121, 21);
            this.combo_com_n2.TabIndex = 0;
            this.combo_com_n2.SelectedIndexChanged += new System.EventHandler(this.combo_com_n2_SelectedIndexChanged);
            // 
            // button_accept
            // 
            this.button_accept.Location = new System.Drawing.Point(140, 145);
            this.button_accept.Name = "button_accept";
            this.button_accept.Size = new System.Drawing.Size(75, 23);
            this.button_accept.TabIndex = 1;
            this.button_accept.Text = "Принять";
            this.button_accept.UseVisualStyleBackColor = true;
            this.button_accept.Click += new System.EventHandler(this.button_accept_Click);
            // 
            // button_discard
            // 
            this.button_discard.Location = new System.Drawing.Point(230, 145);
            this.button_discard.Name = "button_discard";
            this.button_discard.Size = new System.Drawing.Size(75, 23);
            this.button_discard.TabIndex = 1;
            this.button_discard.Text = "Отменить";
            this.button_discard.UseVisualStyleBackColor = true;
            this.button_discard.Click += new System.EventHandler(this.button_discard_Click);
            // 
            // EquipmentSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 182);
            this.Controls.Add(this.button_discard);
            this.Controls.Add(this.button_accept);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EquipmentSettings";
            this.Text = "Оборудование";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_com_n1;
        private System.Windows.Forms.ComboBox combo_com_n1;
        private System.Windows.Forms.Button button_test_n1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_test_n2;
        private System.Windows.Forms.Label label_com_n2;
        private System.Windows.Forms.ComboBox combo_com_n2;
        private System.Windows.Forms.Button button_accept;
        private System.Windows.Forms.Button button_discard;
    }
}