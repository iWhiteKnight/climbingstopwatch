﻿using System;
using System.Collections.Generic;
namespace stopwatch_v1
{
    interface IProtocolProvider
    {
        IDictionary<RoundType, IList<Athlete>> GetAthletesFromBook(string filePath);

        void SaveAthlList(string filePath);
    }
}
