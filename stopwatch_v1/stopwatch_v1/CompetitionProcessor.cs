﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    public class CompetitionProcessor
    {
        
        private IRoundProcessor _roundProcessor;

        private readonly StopWatch _stopWatch;

        public event EventHandler RoundFinished;
        public event EventHandler RoundStarted;
        public event EventHandler CompetitionFinished;
        public event EventHandler RaceFinished;
        public event AthleteEventHandler FalseStart;
        public event AthleteEventHandler Disqualified;
        public event EventHandler<NumberEventArgs> FalseStartNotActive;
        public event EventHandler<NumberEventArgs> FinishIsPressed;
        public event EventHandler RaceStarted;
        public event EventHandler<StopEventArgs> AthleteStopped;

        public Athlete FirstAthl
        {
            get
            {
                return _roundProcessor != null ? _roundProcessor.FirstAthl : null;
            }
        }

        public Athlete SecAthl
        {
            get
            {
                return _roundProcessor != null ? _roundProcessor.SecAthl : null;
            }
        }

        public Round Round { get; private set; }

        public CompetitionProcessor(StopWatch stopWatch)
        {
            _stopWatch = stopWatch;
        }

        public void StartRound(Round round)
        {
            if (Round != null)
            {
                Program.Athletes[round.Type] =
                        new AthleteListSelector().GetNextRoundAthleteList(Program.Athletes[Round.Type],
                                                                          Round.Type, round.Type);
            }
            Round = round;
            _roundProcessor = new RoundProcessor(round, _stopWatch);
            _roundProcessor.RaceFinished += RaceFinished;
            _roundProcessor.FalseStartNotActive += FalseStartNotActive;
            _roundProcessor.FalseStart += FalseStart;
            _roundProcessor.Disqualified += Disqualified;
            _roundProcessor.RaceStarted += RaceStarted;
            _roundProcessor.AthleteStopped += AthleteStopped;
            _roundProcessor.FinishIsPressed += FinishIsPressed;

            RoundStarted(this, EventArgs.Empty);
        }

        public void StartRace()
        {
            _roundProcessor.StartRace();
        }

        public void Transition()
        {
            if (!_roundProcessor.Transit())
            {
                FinishRound();

                //var prevRound = _roundProcessor.Round;

                //var round = GetNextRound();
                //if (round.Type == RoundType.Undefined)
                //{
                //    CompetitionFinished(this, EventArgs.Empty);
                //}
                //else
                //{
                //    Program.Athletes[round.Type] =
                //        new AthleteListSelector().GetNextRoundAthleteList(Program.Athletes[prevRound.Type],
                //                                                          prevRound.Type, round.Type);
                //    StartRound(round);
                //}
            }
        }

        public void Stop(int index, bool isFalseStart, bool isFall)
        {
            _roundProcessor.Stop(index, isFalseStart, isFall);
        }

        public void RestoreLastAthletes(Athlete firstAthlete, Athlete secondAthlete)
        {
            _roundProcessor.RestoreLastAthletes(firstAthlete, secondAthlete);
        }

        private void FinishRound()
        {
            _roundProcessor.RaceFinished -= RaceFinished;
            _roundProcessor.FalseStartNotActive -= FalseStartNotActive;
            _roundProcessor.FalseStart -= FalseStart;
            _roundProcessor.Disqualified -= Disqualified;
            RoundFinished(this, EventArgs.Empty);
        }

    }
}
