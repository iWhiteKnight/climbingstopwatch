﻿namespace stopwatch_v1
{
    public interface IAthletesProvider
    {
        Athlete[] SelectNextAthletes();
    }
}
