﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;
using System.Threading;

namespace stopwatch_v1
{
    public partial class Form_test : Form
    {
        private SerialPort com_port;

        public Form_test(int trace_n, String com_name)
        {
            InitializeComponent();

            // Init label
            if (trace_n == 1)
                label_trace.Text = "Трасса №1";
            else
                label_trace.Text = "Трасса №2";

           
            
            // Открытие порта, если он еще не открыт
            if (com_port == null)
            {
                com_port = new SerialPort(com_name);
                com_port.Handshake = Handshake.RequestToSendXOnXOff;
                com_port.DtrEnable = true;
                com_port.RtsEnable = true;
                try
                {
                    com_port.Open();
                }
                catch (System.IO.IOException)
                {
                    com_port = null;
                    MessageBox.Show("Не удается открыть COM-порт");
                }
                catch(UnauthorizedAccessException)
                {
                    com_port = null;
                    MessageBox.Show("Порт занят");
                }

                if (com_port != null)
                    com_port.PinChanged += new SerialPinChangedEventHandler(com_event);
            }

        }

        private void button_end_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void com_event(object sender, SerialPinChangedEventArgs event_args)
        {
            if (event_args.EventType == SerialPinChange.DsrChanged)
            {
                if (com_port.DsrHolding)
                    //MessageBox.Show("Старт = true");
                    button_start.BackColor = SystemColors.HotTrack;
                else
                    //MessageBox.Show("Старт = false");
                    button_start.BackColor = SystemColors.InactiveBorder;

            }
            else if (event_args.EventType == SerialPinChange.CtsChanged)
            {
                if (com_port.CtsHolding)
                    //MessageBox.Show("Финиш = true");
                    button_finish.BackColor = SystemColors.HotTrack;
                else
                    //MessageBox.Show("Финиш = false");
                    button_finish.BackColor = SystemColors.InactiveBorder;
            }
        }

        private void Form_test_closing(object sender, FormClosingEventArgs e)
        {
            if (com_port != null)
                if (com_port.IsOpen)
                {
                    com_port.Close();

                    com_port = null;
                }
        }
    }
}
