﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    public class Result
    {
        public TimeSpan ResultTime { get; set; }
        public bool Fell { get; set; }
        public bool FalseStart { get; set; }
        public bool Disqualified { get; set; }

        public override string ToString()
        {
            return ResultTime.FormatTimeSpan();
        }
    }
}
