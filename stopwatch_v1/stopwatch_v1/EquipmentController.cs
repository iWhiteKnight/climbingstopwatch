﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace stopwatch_v1
{
    public class EquipmentController
    {
        private static readonly EquipmentController _instance = new EquipmentController();

        public static EquipmentController Instance
        {
            get { return _instance; }
        }

        private EquipmentController()
        {
        }

        public void OpenPort(int traceNumber)
        {
            string errorMessage =
                String.Format("Оборудование трассы №{0} не работает, /n проверьте правильность подключения", traceNumber);
            switch (traceNumber)
            {
                case 1:
                    if (COM_data.com_n1 == null)
                    {
                        COM_data.com_n1 = OpenPort(COM_data.name_n1, errorMessage);
                    }
                    break;

                case 2:
                    if (COM_data.com_n2 == null)
                    {
                        COM_data.com_n2 = OpenPort(COM_data.name_n2, errorMessage);
                    }
                    break;
                
                default: throw new ArgumentException("traceNumber can be 1 or 2");

            }
        }

        private SerialPort OpenPort(string portName, string errorMessage)
        {
            var port = new SerialPort(portName);
            port.Handshake = Handshake.RequestToSendXOnXOff;
            port.DtrEnable = true;
            port.RtsEnable = true;
            try
            {
                port.Open();
            }
            catch (System.IO.IOException)
            {
                throw new Exception(errorMessage);
            }
          
            return port;
        }


        public void ClosePort(int traceNumber)
        {
            switch (traceNumber)
            {
                case 1:
                    if (COM_data.com_n1 != null && COM_data.com_n1.IsOpen)
                    {
                        COM_data.com_n1.Close();
                        COM_data.com_n1 = null;
                    }
                    break;
                case 2:
                    if (COM_data.com_n2 != null && COM_data.com_n2.IsOpen)
                    {
                        COM_data.com_n2.Close();
                        COM_data.com_n2 = null;
                    }
                    break;
            }
        }


        public bool IsPortOpen(int traceNumber)
        {
            switch (traceNumber)
            {
                case 1: return !String.IsNullOrEmpty(COM_data.name_n1);
                case 2: return !String.IsNullOrEmpty(COM_data.name_n2);
                default: throw new ArgumentException("The traceNumber can be 1 or 2");
            }
        }

        public string GetPortName(int traceNumber)
        {
            switch (traceNumber)
            {
                case 1:
                    return COM_data.name_n1;
                case 2:
                    return COM_data.name_n2;
                default:
                    return "Unknown";
            }
        }
    }
}