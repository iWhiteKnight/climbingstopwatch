﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    public static class AthletesProviderFactory
    {
        public static IAthletesProvider GetAthletesProvider(RoundType roundType)
        {
            switch (roundType)
            {
                case RoundType.Qualification:
                    return new ClassicQualificationAthletesProvider(Program.Athletes[roundType]);
                case RoundType.OneEight:
                case RoundType.QuarterFinal:
                case RoundType.HalfFinal:
                case RoundType.Final:
                    return new FinalAthletesProvider(Program.Athletes[roundType]);
                default:
                    throw new ArgumentException("Unknown round type");
            }
        }
    }
}
