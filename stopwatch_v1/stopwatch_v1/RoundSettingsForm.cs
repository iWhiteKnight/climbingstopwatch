﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace stopwatch_v1
{
    public partial class RoundSettingsForm : Form
    {
        public event EventHandler<RoundSettingsEventArgs> RoundSelected;

        public RoundSettingsForm(RoundType roundType)
        {
            InitializeComponent();
            switch (GetNextRoundType(roundType))
            {
                case RoundType.Qualification:
                    radioButtonQual.Checked = true;
                    radioButtonEighth.Enabled = false;
                    radioButtonForth.Enabled = false;
                    radioButtonHalf.Enabled = false;
                    radioButtonFinal.Enabled = false;
                    break;
                case RoundType.OneEight:
                    radioButtonQual.Enabled = false;
                    radioButtonEighth.Checked = true;
                    break;
                case RoundType.QuarterFinal:
                    radioButtonQual.Enabled = false;
                    radioButtonEighth.Enabled = false;
                    radioButtonForth.Checked = true;
                    break;
                case RoundType.HalfFinal:
                    radioButtonQual.Enabled = false;
                    radioButtonEighth.Enabled = false;
                    radioButtonForth.Enabled = false;
                    radioButtonHalf.Checked = true;
                    break;
                case RoundType.Final:
                    radioButtonQual.Enabled = false;
                    radioButtonEighth.Enabled = false;
                    radioButtonForth.Enabled = false;
                    radioButtonHalf.Enabled = false;
                    radioButtonFinal.Checked = true;
                    break;
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_begin_Click(object sender, EventArgs e)
        {
            RoundType roundType;

            if (radioButtonQual.Checked)
                roundType = RoundType.Qualification;
            else if (radioButtonEighth.Checked)
                roundType = RoundType.OneEight;
            else if (radioButtonForth.Checked)
                roundType = RoundType.QuarterFinal;
            else if (radioButtonHalf.Checked)
                roundType = RoundType.HalfFinal;
            else
                roundType = RoundType.Final;

            RoundSelected(this, new RoundSettingsEventArgs {IsMale = radioButtonMale.Checked, RoundType = roundType});

            Close();
        }

        private RoundType GetNextRoundType(RoundType roundType)
        {
            if (roundType == RoundType.Qualification)
            {
                int count = Program.Athletes[RoundType.Qualification].Count(athl => !athl.IsDisqualified && !athl.IsFallen);
                if (count >= 16)
                {
                    return RoundType.OneEight;
                }
                if (count >= 8)
                {
                    return RoundType.QuarterFinal;
                }
                if (count >= 4)
                {
                    return RoundType.HalfFinal;
                }
            }
            return (RoundType)(((int)roundType + 1) % ((int)RoundType.Final + 1));
        }
    }

    public class RoundSettingsEventArgs : EventArgs
    {
        public RoundType RoundType { get; set; }
        public bool IsMale { get; set; }
    }
}
