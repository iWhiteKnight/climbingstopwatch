namespace stopwatch_v1
{
    public interface IAthletesProvider
    {
        Athlete[] SelectNextAthletes();

        void SetLastActivePair(Athlete firstAthlete, Athlete secondAthlete);
    }
}