﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.SS.UserModel;
using log4net;


namespace stopwatch_v1
{
    class ClassicExcelProtocolProvider : IProtocolProvider
    {
        private HSSFWorkbook _saveWorkBook;

        private static readonly ILog _logger = LogManager.GetLogger(typeof(ClassicExcelProtocolProvider));

        public IDictionary<RoundType, IList<Athlete>> GetAthletesFromBook(string filePath)
        {
            var excelApp = new Application();

            Workbook readWorkBook = excelApp.Workbooks.Open(filePath,
                                         Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                         Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                         Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                         Type.Missing, Type.Missing);

            try
            {
                var athletes = new Dictionary<RoundType, IList<Athlete>>();

                for (int i = 1; i <= readWorkBook.Worksheets.Count; i++)
                {
                    var rawsNum = GetRawsCount(readWorkBook.Worksheets[i]);
                    Range range = (Range) readWorkBook.Worksheets[i].Range["A3", "N" + rawsNum];
                    object[,] array = range.Value;

                    var roundAthletes = new List<Athlete>();

                    for (int j = 1; j < rawsNum - 1; j++)
                    {
                        //TODO: this is just workaround
                        if (array[j, 2] == null)
                        {
                            break;
                        }
                        var athlete = new Athlete(Convert.ToInt32(array[j, 2]));
                        athlete.ResultT1Cl = GetResultFromCells(array[j, 6], array[j, 9], array[j, 11], array[j, 13]);
                        athlete.ResultT2Cl = GetResultFromCells(array[j, 7], array[j, 10], array[j, 12],
                                                                array[j, 14]);

                        athlete.FIO = (array[j, 3] ?? "Н/Д").ToString();
                        roundAthletes.Add(athlete);

                    }

                    RoundType type;
                    try
                    {
                        //If doc was saved automatically via this app
                        type = Enum.Parse(typeof(RoundType), readWorkBook.Worksheets[i].Name);
                    }
                    catch (ArgumentException)
                    {
                        //In case of manual doc - this occurs on first launch.
                        type = (RoundType) i;
                    }
                    athletes.Add(type, roundAthletes);
                }

                return athletes;
            }
            finally
            {
                readWorkBook.Close();
                excelApp.Quit();
            }
        }

        private Result GetResultFromCells(object timeCell, object fallCell, object falseStartCell, object disqualCell)
        {
            return new Result
                       {
                           ResultTime = ((timeCell ?? "").ToString()).ToTimeSpan(),
                           Fell = (fallCell ?? "0").ToString().ToBool(),
                           FalseStart = (falseStartCell ?? "0").ToString().ToBool(),
                           Disqualified = (disqualCell ?? "0").ToString().ToBool(),
                       };
        }

        private int GetRawsCount(Worksheet sheet)
        {
            return sheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing).Row;
        }

        public void SaveAthlList(string filePath)
        {
            IntitalizeSaveWB(ref _saveWorkBook);

            IDictionary<RoundType, IList<Athlete>> athletes = Program.Athletes;
            foreach (RoundType type in Enum.GetValues(typeof(RoundType)))
            {
                if (athletes.ContainsKey(type))
                {
                    IList<Athlete> roundAthletes = athletes[type];
                    ISheet roundSheet = _saveWorkBook.CreateSheet(type.ToString());

                    IRow headerRow = roundSheet.CreateRow(0);
                    headerRow.CreateCell(1).SetCellValue("идент #");
                    headerRow.CreateCell(2).SetCellValue("ФИО");
                    headerRow.CreateCell(5).SetCellValue("r1");
                    headerRow.CreateCell(6).SetCellValue("r2");
                    headerRow.CreateCell(7).SetCellValue("сумма");
                    headerRow.CreateCell(8).SetCellValue("срыв1");
                    headerRow.CreateCell(9).SetCellValue("срыв2");
                    headerRow.CreateCell(10).SetCellValue("фальстарт1");
                    headerRow.CreateCell(11).SetCellValue("фальстарт2");
                    headerRow.CreateCell(12).SetCellValue("дисквал1");
                    headerRow.CreateCell(13).SetCellValue("дисквал2");

                    var i = 2; //Start from A3
                    foreach (var athlete in roundAthletes)
                    {
                        var athleteRow = roundSheet.CreateRow(i);
                        athleteRow.CreateCell(1).SetCellValue(athlete.AthleteID);
                        athleteRow.CreateCell(2).SetCellValue(athlete.FIO);
                        athleteRow.CreateCell(5).SetCellValue(athlete.ResultT1Cl.ResultTime.FormatTimeSpan());
                        athleteRow.CreateCell(6).SetCellValue(athlete.ResultT2Cl.ResultTime.FormatTimeSpan());
                        athleteRow.CreateCell(7).SetCellValue(athlete.ResultTSumClassic.FormatTimeSpan());
                        athleteRow.CreateCell(8).SetCellValue(athlete.ResultT1Cl.Fell.ToIntString());
                        athleteRow.CreateCell(9).SetCellValue(athlete.ResultT2Cl.Fell.ToIntString());
                        athleteRow.CreateCell(10).SetCellValue(athlete.ResultT1Cl.FalseStart.ToIntString());
                        athleteRow.CreateCell(11).SetCellValue(athlete.ResultT2Cl.FalseStart.ToIntString());
                        athleteRow.CreateCell(12).SetCellValue(athlete.ResultT1Cl.Disqualified.ToIntString());
                        athleteRow.CreateCell(13).SetCellValue(athlete.ResultT2Cl.Disqualified.ToIntString());
                        i++;

                    }
                }
            }



            if (_saveWorkBook != null)
            {
                WriteToFile(_saveWorkBook, filePath);
            }
        }

        private static void WriteToFile(HSSFWorkbook workBook, string filePath)
        {
            try
            {
                using (FileStream file = new FileStream(filePath, FileMode.Create))
                {
                    workBook.Write(file);
                }
            }
            catch (IOException)
            {
                throw new IOException(String.Format("Файл {0} используется другим процессом и не может быть сохранен. Пожалуйста, закройте файл.", filePath));
            }

        }


        private static void IntitalizeSaveWB(ref HSSFWorkbook workBook)
        {
            workBook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "MinskOnsight";

            workBook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Speed Competition";
            workBook.SummaryInformation = si;
        }

    }
}
