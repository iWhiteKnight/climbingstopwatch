﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO.Ports;              // для COM
using System.Threading;
using log4net;
using log4net.Config;


namespace stopwatch_v1
{
    static class Program
    {
        private static ILog _logger;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        [STAThread]
        static void Main()
        {
            XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(typeof (Program));

            _logger.Info("----- Entering application -------");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());

            _logger.Info("----- Exiting application -----" + Environment.NewLine);
        }

        public static IDictionary<RoundType, IList<Athlete>> Athletes { get; set; }

    }


    static class Heat_data
    {
        // Трасса №1
        public static bool false_n1 = false;
        public static bool fail_n1 = false;
        public static TimeSpan result_n1;

        // Трасса №2
        public static bool false_n2 = false;
        public static bool fail_n2 = false;
        public static TimeSpan result_n2;

    }

    static class COM_data
    {
        public static String name_n1 = "";
        public static String name_n2 = "";
        public static SerialPort com_n1;
        public static SerialPort com_n2;

        public static SerialPort COM_Open(SerialPort com_port, String com_name)
        {
            if (com_port == null)
            {
                com_port = new SerialPort(com_name);
                com_port.Handshake = Handshake.RequestToSendXOnXOff;
                com_port.DtrEnable = true;
                com_port.RtsEnable = true;

                try
                {
                    com_port.Open();
                }
                catch (System.IO.IOException)
                {
                    string str = String.Format("{0} {1}", "Не удается открыть порт", com_name);
                    com_port = null;
                    MessageBox.Show(str);
                }
                catch (System.UnauthorizedAccessException)
                {
                    string str = String.Format("{0} {1} {2}", "Порт", com_name, "занят");
                    com_port = null;
                    MessageBox.Show(str);
                }
            }

            return com_port;
        }
    }
}
