﻿namespace stopwatch_v1
{
    public interface IDisplayable
    {
        void Display(string text);
    }
}
