﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class EquipmentReadyState : StateBase
    {
        public EquipmentReadyState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
            _mainForm.meny_equipment.Enabled = true;
            _mainForm.attachProtocolToolStripMenuItem.Enabled = true;
        }

        public override StateBase AttachProtocol()
        {
            return new ProtocolAttachedState(_mainForm);
        }
    }
}
