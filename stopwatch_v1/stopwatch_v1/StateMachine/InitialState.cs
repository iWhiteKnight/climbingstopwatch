﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class InitialState : StateBase
    {
        public InitialState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
            _mainForm.meny_equipment.Enabled = true;
        }

        public override StateBase SetEquipment()
        {
            return new EquipmentReadyState(_mainForm);
        }
    }
}
