﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class SecondWaitingState : StateBase
    {
        public SecondWaitingState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
        }

        public override StateBase Run()
        {
            return new SecondRunningState(_mainForm);
        }

        public override StateBase FalseStart()
        {
            return new SecondReadyState(_mainForm);
        }

        public override StateBase Disqualify()
        {
            return new SecondStoppedState(_mainForm);
        }

        public override StateBase CannotStart()
        {
            return new SecondReadyState(_mainForm);
        }
    }
}
