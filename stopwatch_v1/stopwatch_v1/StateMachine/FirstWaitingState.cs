﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class FirstWaitingState : StateBase
    {
        public FirstWaitingState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
        }

        public override StateBase Run()
        {
            return new FirstRunningState(_mainForm);
        }

        public override StateBase FalseStart()
        {
            return new FirstReadyState(_mainForm);
        }

        public override StateBase Disqualify()
        {
            return new FirstStoppedState(_mainForm);
        }

        public override StateBase CannotStart()
        {
            return new FirstReadyState(_mainForm);
        }
    }
}
