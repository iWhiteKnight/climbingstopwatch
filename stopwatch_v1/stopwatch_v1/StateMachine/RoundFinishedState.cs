﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class RoundFinishedState : StateBase
    {
        public RoundFinishedState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
            _mainForm.buttonStartRound.Enabled = true;

            _mainForm.reorderButton.Enabled = true;
            _mainForm.saveButton.Enabled = true;
        }

        public override StateBase StartRound()
        {
            return new RoundStartedState(_mainForm);
        }

        public override StateBase EditProtocol(IList<Athlete> athletes, int selectedIndex)
        {
            return new ProtocolEditingState(_mainForm, this, athletes, selectedIndex);
        }
    }
}
