﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    abstract class StateBase
    {
        protected MainForm _mainForm;

        public StateBase(MainForm mainForm)
        {
            _mainForm = mainForm;
        }

        public void UpdateUI()
        {
            _mainForm.button_stop_n1.Enabled = false;
            _mainForm.button_stop_n2.Enabled = false;
            _mainForm.button_fail_n1.Enabled = false;
            _mainForm.button_fail_n2.Enabled = false;

            _mainForm.button_start.Enabled = false;

            _mainForm.button_trans.Enabled = false;

            _mainForm.buttonStartRound.Enabled = false;

            _mainForm.meny_equipment.Enabled = false;
            _mainForm.attachProtocolToolStripMenuItem.Enabled = false;
            _mainForm.detachProtocolToolStripMenuItem.Enabled = false;

            _mainForm.reorderButton.Enabled = false;
            _mainForm.saveButton.Enabled = false;

            _mainForm.upButton.Enabled = false;
            _mainForm.downButton.Enabled = false;
            _mainForm.removeButton.Enabled = false;

            UpdateSpecial();
        }

        protected abstract void UpdateSpecial();

        public virtual StateBase SetEquipment()
        {
            return this;
        }

        public virtual StateBase AttachProtocol()
        {
            return this;
        }

        public virtual StateBase TransitAthletes(Athlete athl1, Athlete athl2)
        {
            return this;
        }

        public virtual StateBase StartRound()
        {
            return this;
        }

        public virtual StateBase StartWaiting()
        {
            return this;
        }

        public virtual StateBase Run()
        {
            return this;
        }

        public virtual StateBase FalseStart()
        {
            return this;
        }

        public virtual StateBase StopFirst()
        {
            return this;
        }

        public virtual StateBase StopSecond()
        {
            return this;
        }

        public virtual StateBase Disqualify()
        {
            return this;
        }

        public virtual StateBase CannotStart()
        {
            return this;
        }

        public virtual StateBase EditProtocol(IList<Athlete> athletes, int selectedIndex)
        {
            return this;
        }

        public virtual StateBase EditProtocolItem(IList<Athlete> athletes, int selectedIndex)
        {
            return this;
        }
    }
}
