﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class BothReadyState : StateBase
    {
        public BothReadyState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
            _mainForm.button_start.Enabled = true;
            _mainForm.saveButton.Enabled = true;
        }

        public override StateBase StartWaiting()
        {
            return new BothWaitingState(_mainForm);
        }
    }
}
