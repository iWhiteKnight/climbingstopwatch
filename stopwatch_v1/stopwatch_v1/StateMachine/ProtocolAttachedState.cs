﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class ProtocolAttachedState : StateBase
    {
        public ProtocolAttachedState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
            _mainForm.buttonStartRound.Enabled = true;
        }

        public override StateBase StartRound()
        {
            return new RoundStartedState(_mainForm);
        }

        public override StateBase EditProtocol(IList<Athlete> athletes, int selectedIndex)
        {
            return new ProtocolEditingState(_mainForm, this, athletes, selectedIndex);
        }
    }
}
