﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class BothWaitingState : StateBase
    {
        public BothWaitingState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
        }

        public override StateBase Run()
        {
            return new BothRunningState(_mainForm);
        }

        public override StateBase FalseStart()
        {
            return new BothReadyState(_mainForm);
        }

        public override StateBase Disqualify()
        {
            return new BothStoppedState(_mainForm);
        }

        public override StateBase CannotStart()
        {
            return new BothReadyState(_mainForm);
        }
    }
}
