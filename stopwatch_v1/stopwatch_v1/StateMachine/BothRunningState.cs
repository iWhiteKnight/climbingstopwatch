﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class BothRunningState : StateBase
    {
        public BothRunningState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
            _mainForm.button_stop_n1.Enabled = true;
            _mainForm.button_stop_n2.Enabled = true;
            _mainForm.button_fail_n1.Enabled = true;
            _mainForm.button_fail_n2.Enabled = true;
        }

        public override StateBase StopFirst()
        {
            return new FirstStopSecRunState(_mainForm);
        }

        public override StateBase StopSecond()
        {
            return new FirstRunSecStopState(_mainForm);
        }
    }
}
