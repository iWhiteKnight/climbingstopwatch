﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class ProtocolEditingState : StateBase
    {
        private StateBase _prevState;

        private IList<Athlete> _athletes;

        private int _selectedIndex;

        public ProtocolEditingState(MainForm mainForm, StateBase prevState, IList<Athlete> athletes, int selectedIndex)
            : base(mainForm)
        {
            _prevState = prevState;
            _athletes = athletes;
            _selectedIndex = selectedIndex;
        }

        protected override void UpdateSpecial()
        {
            _mainForm.reorderButton.Enabled = true;

            if (_selectedIndex >= 0 && _athletes[_selectedIndex].CanBeSelected)
            {
                if (_selectedIndex > 0 && _athletes[_selectedIndex - 1].CanBeSelected)
                {
                    _mainForm.upButton.Enabled = true;
                }
                if (_selectedIndex + 1 <= _athletes.Count - 1 && _athletes[_selectedIndex + 1].CanBeSelected)
                {
                    _mainForm.downButton.Enabled = true;
                }
                _mainForm.removeButton.Enabled = true;
            }
        }

        public override StateBase EditProtocol(IList<Athlete> athletes, int selectedIndex)
        {
            return _prevState;
        }

        public override StateBase EditProtocolItem(IList<Athlete> athletes, int selectedIndex)
        {
            return new ProtocolEditingState(_mainForm, _prevState, athletes, selectedIndex);
        }
    }
}
