﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1.StateMachine
{
    class FirstStoppedState : StateBase
    {
        public FirstStoppedState(MainForm mainForm) : base(mainForm)
        {
        }

        protected override void UpdateSpecial()
        {
            _mainForm.button_trans.Enabled = true;

            _mainForm.reorderButton.Enabled = true;
            _mainForm.saveButton.Enabled = true;
        }

        public override StateBase TransitAthletes(Athlete athl1, Athlete athl2)
        {
            if (athl1 != null && athl2 != null)
            {
                return new BothReadyState(_mainForm);
            }
            if (athl1 != null)
            {
                return new FirstReadyState(_mainForm);
            }
            if (athl2 != null)
            {
                return new SecondReadyState(_mainForm);
            }
            return new RoundFinishedState(_mainForm);
        }

        public override StateBase EditProtocol(IList<Athlete> athletes, int selectedIndex)
        {
            return new ProtocolEditingState(_mainForm, this, athletes, selectedIndex);
        }
    }
}
