﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using log4net;
using stopwatch_v1.StateMachine;


namespace stopwatch_v1
{
    public partial class MainForm : Form
    {
        private readonly CompetitionProcessor _processor;

        private static ILog _logger = LogManager.GetLogger(typeof(MainForm));

        private bool _gridEditingMode;

        private StateBase _state;

        private IList<Athlete> _curList;


        // -------------------------------------------------------------------------------------
        public MainForm()
        {
            InitializeComponent();

            _processor = new CompetitionProcessor(new StopWatch(new LabelDisplayable(label_time_n1), new LabelDisplayable(label_time_n2)));
            _processor.RoundFinished += OnRoundFinished;
            _processor.RoundStarted += OnRoundStarted;

            _processor.FalseStart += OnFalseStart;
            _processor.Disqualified += OnDisqualified;
            _processor.RaceFinished += OnRaceFinished;
            _processor.FalseStartNotActive += OnFalsStartIsNotActive;
            _processor.FinishIsPressed += OnFinishIsPressed;
            _processor.RaceStarted += OnRaceStarted;
            _processor.AthleteStopped += OnAthleteStopped;

            _processor.CompetitionFinished += OnCompetitionFinished;

            _state = new InitialState(this);
        }


        private void OnCompetitionFinished(object sender, EventArgs e)
        {
            MessageBox.Show("Соревнования завершены!");
        }

        // -------------------------------------------------------------------------------------
        private void Form_main_Load(object sender, EventArgs e)
        {
            TryLoadPortsFromSettings();
            AnalyzePorts();
        }

        // -------------------------------------------------------------------------------------

        // -------------------------------------------------------------------------------------
        private void meny_equipment_settings_Click(object sender, EventArgs e)
        {
            EquipmentSettings equipment = new EquipmentSettings();
            equipment.ShowDialog();
            AnalyzePorts();
        }

        private void buttonStartRound_Click(object sender, EventArgs e)
        {
            RoundType roundType = _processor.Round == null ? RoundType.Undefined : _processor.Round.Type;
            var form = new RoundSettingsForm(roundType);
            form.RoundSelected += delegate(object o, RoundSettingsEventArgs args)
                                      {
                                          var round = new Round(args.RoundType, args.IsMale);
                                          _processor.StartRound(round);
                                      };
            form.ShowDialog(this);
        }

        private DataGridView SelectDataGrid(RoundType type)
        {
            DataGridView dataGrid;
            switch (type)
            {
                case RoundType.Qualification: dataGrid = dataGridQual;
                    break;
                case RoundType.OneEight: dataGrid = dataGridEight;
                    break;
                case RoundType.QuarterFinal: dataGrid = dataGridFour;
                    break;
                case RoundType.HalfFinal: dataGrid = dataGridHalf;
                    break;
                case RoundType.Final: dataGrid = dataGridFinal;
                    break;
                default: dataGrid = null;
                    break;
            }

            return dataGrid;
        }

        private void RefreshDataGrid()
        {
            foreach (var key in Program.Athletes.Keys)
            {
                var roundAthletes = Program.Athletes[key];
                var dataGrid = SelectDataGrid(key);
                SetupDataGrid(dataGrid, roundAthletes);
            }

        }


        private void OnSelectedRowChanged(object sender, EventArgs e)
        {
            int index = GetCurrentRowIndex();
            _state = _state.EditProtocolItem(Program.Athletes[_processor.Round.Type], index);
            _state.UpdateUI();
        }


        private void SetupDataGrid(DataGridView dataGrid, IList<Athlete> list)
        {
            if (dataGrid != null)
            {
                dataGrid.AutoGenerateColumns = false;
                dataGrid.Columns.Clear();

                var column = new DataGridViewTextBoxColumn {DataPropertyName = "FIO", Name = "ФИО"};
                dataGrid.Columns.Add(column);

                column = new DataGridViewTextBoxColumn { DataPropertyName = "ResultT1Cl", Name = "Результат №1" };
                dataGrid.Columns.Add(column);

                column = new DataGridViewTextBoxColumn { DataPropertyName = "ResultT2Cl", Name = "Результат №2" };
                dataGrid.Columns.Add(column);

                column = new DataGridViewTextBoxColumn { DataPropertyName = "ResultSumString", Name = "Общий результат" };
                dataGrid.Columns.Add(column);

                var cbColumn = new DataGridViewCheckBoxColumn { DataPropertyName = "HasFalsStart", Name = "Был фальстарт" };
                dataGrid.Columns.Add(cbColumn);

                cbColumn = new DataGridViewCheckBoxColumn { DataPropertyName = "IsDisqualified", Name = "Дисквалифицирован" };
                dataGrid.Columns.Add(cbColumn);

                cbColumn = new DataGridViewCheckBoxColumn { DataPropertyName = "IsFallen", Name = "Срыв" };
                dataGrid.Columns.Add(cbColumn);

//                dataGrid.Columns[0].Visible = false;
//                dataGrid.Columns[1].HeaderText = "ФИО";
//                dataGrid.Columns[2].HeaderText = "Результат №1";
//                dataGrid.Columns[3].HeaderText = "Результат №2";
//                dataGrid.Columns[4].HeaderText = "Общий результат";
//                dataGrid.Columns[5].Visible = false;
//                dataGrid.Columns[6].HeaderText = "Был фальстарт";
//                dataGrid.Columns[7].HeaderText = "Дисквалифицирован";
//                dataGrid.Columns[8].HeaderText = "Срыв";
//                dataGrid.Columns[9].Visible = false;

                BindingSource bindingSource = new BindingSource(list, "");

                dataGrid.DataSource = bindingSource;

            }
        }

        IProtocolProvider _provider;

        private void AttachProtocol_Click(object sender, EventArgs e)
        {
            string protocolFilePath = string.Empty;
            if (openProtocol.ShowDialog() == DialogResult.OK)
            {
                protocolFilePath = openProtocol.FileName;
                lblProtocol.Text = Path.GetFileName(protocolFilePath);
                
                _provider = new ClassicExcelProtocolProvider();
                Program.Athletes = _provider.GetAthletesFromBook(protocolFilePath);

                RefreshDataGrid();

                _state = _state.AttachProtocol();
                _state.UpdateUI();
                UpdateUI();

                if (Program.Athletes[RoundType.Qualification].Count > 0 && 
                    Program.Athletes[RoundType.Qualification][0].HasResult)
                {
                    //continue previous competition
                    RoundType roundType = DefineSavedRoundType();
                    _processor.StartRound(new Round(roundType, true));

                    RestoreSavedState(roundType);
                }
            }
        }

        private RoundType DefineSavedRoundType()
        {
            RoundType result = RoundType.Qualification;
            foreach (RoundType type in Enum.GetValues(typeof(RoundType)))
            {
                if (type == RoundType.Undefined)
                {
                    continue;
                }
                if (Program.Athletes.ContainsKey(type) && Program.Athletes[type][0].HasResult)
                {
                    result = type;
                }
            }
            return result;
        }

        private void RestoreSavedState(RoundType roundType)
        {
            Athlete firstAthl;
            Athlete secondAthl;
            if (roundType == RoundType.Qualification)
            {
                int activeIndex = DefineLastActiveAthleteIndex(Program.Athletes[roundType]);
                if (activeIndex == Program.Athletes[roundType].Count - 1 && Program.Athletes[roundType][activeIndex].FinishedRound)
                {
                    firstAthl = null;
                    secondAthl = Program.Athletes[roundType][activeIndex];
                    _state = new SecondStoppedState(this);
                }
                else
                {
                    firstAthl = Program.Athletes[roundType][activeIndex];
                    if (activeIndex == 0)
                    {
                        secondAthl = null;
                        _state = new FirstStoppedState(this);
                    }
                    else
                    {
                        secondAthl = Program.Athletes[roundType][activeIndex - 1];
                        _state = new BothStoppedState(this);
                    }
                }
            }
            else
            {
                int activeIndex = DefineLastActiveAthleteIndex(Program.Athletes[roundType]);

                if (Program.Athletes[roundType][activeIndex].FinishedRound)
                {
                    firstAthl = Program.Athletes[roundType][activeIndex];
                    secondAthl = Program.Athletes[roundType][activeIndex - 1];
                }
                else
                {
                    firstAthl = Program.Athletes[roundType][activeIndex - 1];
                    secondAthl = Program.Athletes[roundType][activeIndex];
                }
                _state = new BothStoppedState(this);
            }

            _processor.RestoreLastAthletes(firstAthl, secondAthl);
            _state.UpdateUI();
        }

        private int DefineLastActiveAthleteIndex(IList<Athlete> athletes)
        {
            int result = 0;
            for (int i = 0; i < athletes.Count; i++ )
            {
                if (athletes[i].HasResult)
                {
                    result = i;
                }
                else
                {
                    break;
                }
            }
            return result;
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            _state = _state.StartWaiting();
            _state.UpdateUI();
            UpdateUI();
            _processor.StartRace();
        }


        private void OnRaceStarted(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate
                                         {
                                             _state = _state.Run();
                                             _state.UpdateUI();
                                             UpdateUI();
                                         }));
        }

        private void button_stop_n1_Click(object sender, EventArgs e)
        {
            _processor.Stop(1, false, false);
        }

        private void button_stop_n2_Click(object sender, EventArgs e)
        {
            _processor.Stop(2, false, false);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Вы действительно хотите выйти?", "Выход", MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {
                //_processor.Stop(1, false);
                //_processor.Stop(2, false);
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void TryLoadPortsFromSettings()
        {
            try
            {
                COM_data.name_n1 = Properties.Settings.Default.Com1;
                COM_data.name_n2 = Properties.Settings.Default.Com2;
            }
            catch (Exception ex)
            {
                _logger.Error("Error while loading ports", ex);
                throw;
            }
        }

        private void AnalyzePorts()
        {
            if (AnalyzePort(1) && AnalyzePort(2))
            {
                _state = _state.SetEquipment();
                _state.UpdateUI();
            }
        }

        private bool AnalyzePort(int index)
        {
            Label portLbl;
            switch (index)
            {
                case 1:
                    portLbl = comPortLbl1;
                    break;
                case 2:
                    portLbl = comPortLbl2;
                    break;
                default:
                    throw new ArgumentException("Trace number must be 1 or 2");
            }
            portLbl.Visible = true;
            if (EquipmentController.Instance.IsPortOpen(index))
            {
                portLbl.ForeColor = Color.Black;
                portLbl.Text = EquipmentController.Instance.GetPortName(index);
                return true;
            }
            else
            {
                portLbl.ForeColor = Color.Red;
                portLbl.Text = "Не выбран COM порт";
                return false;
            }
        }



        private void OnRoundFinished(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("Этап {0} завершен", _processor.Round.Type.GetString()));
        }

        private void OnRoundStarted(object sender, EventArgs e)
        {
            if (_processor.Round != null)
            {
                _curDataGrid = SelectDataGrid(_processor.Round.Type);
            }
            if (Program.Athletes == null || Program.Athletes.Count == 0)
                MessageBox.Show("Подключите заполненный стартовый протокол");

            RefreshDataGrid();

            label_stage.Text = _processor.Round.Type.GetString();

            tabControl.SelectTab((int)_processor.Round.Type - 1);

            _state = _state.StartRound();
            _state.UpdateUI();
            UpdateUI();
        }

        private DataGridView _curDataGrid;

        private void reorderButton_Click(object sender, EventArgs e)
        {
            if (!_gridEditingMode)
            {
                if (_processor.Round != null)
                {
                    tabControl.SelectedTab = tabControl.TabPages[(int) _processor.Round.Type - 1];
                    _curDataGrid.CurrentCellChanged += OnSelectedRowChanged;
                    _curList = Program.Athletes[_processor.Round.Type];
                    _curDataGrid.GridColor = Color.Crimson;
                    
                    _gridEditingMode = true;
                }
                else
                {
                    MessageBox.Show("Начните Этап");
                }

            }
            else
            {
                _curDataGrid.CurrentCellChanged -= OnSelectedRowChanged;
                _curDataGrid.GridColor = SystemColors.ControlDark;
                _gridEditingMode = false;
            }


            _state = _state.EditProtocol(_curList, GetCurrentRowIndex());
            _state.UpdateUI();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveXlsFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (_processor.Round != null)
                {
                    _curList = null;
                }
                else MessageBox.Show("Начните Этап");

                if (_provider != null)
                {
                    try
                    {
                        _provider.SaveAthlList(saveXlsFileDialog.FileName);
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        public void button_trans_Click(object sender, EventArgs e)
        {
            _processor.Transition();
            _state = _state.TransitAthletes(_processor.FirstAthl, _processor.SecAthl);

            label_time_n1.Text = TimeSpan.Zero.FormatTimeSpan();
            label_time_n2.Text = TimeSpan.Zero.FormatTimeSpan();

            _state.UpdateUI();
            UpdateUI();
        }


        private void UpdateUI()
        {
            if (_processor.FirstAthl != null)
            {
                firstTraceAthlLabel.Visible = true;
                firstTraceAthlLabel.Text = _processor.FirstAthl.FIO;
                if (!_processor.FirstAthl.IsDisqualified && !_processor.FirstAthl.HasFalsStart)
                    firstTraceAthlLabel.BackColor = Color.Transparent;
            }
            else
            {
                firstTraceAthlLabel.Visible = false;
            }

            if (_processor.SecAthl != null)
            {
                secTraceAthlLabel.Visible = true;
                secTraceAthlLabel.Text = _processor.SecAthl.FIO; 
                if (!_processor.SecAthl.IsDisqualified && !_processor.SecAthl.HasFalsStart)
                    secTraceAthlLabel.BackColor = Color.Transparent;
            }
            else
            {
                secTraceAthlLabel.Visible = false;                
            }
         
            if (_curDataGrid != null)
            {
                _curDataGrid.Refresh();
            }
        }

        internal void OnFalsStartIsNotActive(object sender, NumberEventArgs args)
        {
            MessageBox.Show(string.Format("Платформа на трассе N{0} не нажата!", args.Number));
            _state = _state.CannotStart();
            _state.UpdateUI();
        }

        private void OnFinishIsPressed(object sender, NumberEventArgs args)
        {
            MessageBox.Show(string.Format("Приведите датчик финиша на трассе N{0} в исходное положение!", args.Number));
            _state = _state.CannotStart();
            _state.UpdateUI();
        }

        internal void OnFalseStart(Athlete a)
        {
            Invoke(new MethodInvoker(delegate
                                         {
                                             if (a.Equals(_processor.FirstAthl))
                                                 firstTraceAthlLabel.BackColor = Color.Yellow;
                                             else if (a.Equals(_processor.SecAthl))
                                                 secTraceAthlLabel.BackColor = Color.Yellow;

                                             _state = _state.FalseStart();
                                             _state.UpdateUI();
                                             UpdateUI();
                                         }));
        }

        internal void OnDisqualified(Athlete a)
        {
            Invoke(new MethodInvoker(delegate
                                         {
                                             if (a.Equals(_processor.FirstAthl))
                                                 firstTraceAthlLabel.BackColor = Color.Red;
                                             else if (a.Equals(_processor.SecAthl))
                                                 secTraceAthlLabel.BackColor = Color.Red;

                                             _state = _state.Disqualify();
                                             _state.UpdateUI();
                                             UpdateUI();
                                         }));
        }

        internal void OnRaceFinished(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate
                                         {

                                             UpdateUI();
                                             Refresh();
                                         }));
        }

        private void OnAthleteStopped(object sender, StopEventArgs e)
        {
            Invoke(new MethodInvoker(delegate
                                         {
                                             switch (e.Index)
                                             {
                                                 case 1:
                                                     _state = _state.StopFirst();
                                                     break;
                                                 case 2:
                                                     _state = _state.StopSecond();
                                                     break;
                                             }
                                             _state.UpdateUI();
                                         }));
        }

        private void button_fail_n1_Click(object sender, EventArgs e)
        {
            _processor.Stop(1, false, true);
        }

        private void button_fail_n2_Click(object sender, EventArgs e)
        {
            _processor.Stop(2, false, true);
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            int index = GetCurrentRowIndex();
            Athlete tmpAthlete = _curList[index];
            _curList[index] = _curList[index + 1];
            _curList[index + 1] = tmpAthlete;
            _curDataGrid.CurrentCell = _curDataGrid.Rows[index + 1].Cells[0];
            _curDataGrid.Refresh();

            _state = _state.EditProtocolItem(_curList, GetCurrentRowIndex());
            _state.UpdateUI();
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            int index = _curDataGrid.CurrentCell.RowIndex;
            Athlete tmpAthlete = _curList[index];
            _curList[index] = _curList[index - 1];
            _curList[index - 1] = tmpAthlete;
            _curDataGrid.CurrentCell = _curDataGrid.Rows[index - 1].Cells[0];
            _curDataGrid.Refresh();

            _state = _state.EditProtocolItem(_curList, GetCurrentRowIndex());
            _state.UpdateUI();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            //_curList.RemoveAt(_curDataGrid.CurrentCell.RowIndex);
            _curDataGrid.Rows.RemoveAt(_curDataGrid.CurrentCell.RowIndex);
            _curDataGrid.Refresh();

            _state = _state.EditProtocolItem(_curList, GetCurrentRowIndex());
            _state.UpdateUI();
        }

        private int GetCurrentRowIndex()
        {
            if (_curDataGrid == null || _curDataGrid.CurrentCell == null)
            {
                return -1;
            }
            return _curDataGrid.CurrentCell.RowIndex;
        }
    }
}
