﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    public class AthleteListSelector
    {
        private int[] oneEightIndexes = new int[] { 0, 15, 7, 8, 3, 12, 4, 11, 1, 14, 6, 9, 2, 13, 5, 10 };
        private int[] quarterFinalIndexes = new int[] { 0, 7, 3, 4, 1, 6, 2, 5 };
        private int[] halfFinalIndexes = new int[] {0, 3, 1, 2};
        private int[] finalIndexes = new int[] { 2, 3, 0, 1};

        public IList<Athlete> GetNextRoundAthleteList(IList<Athlete> source, RoundType fromRoundType, RoundType toRoundType)
        {
            int[] indexes;
            switch (toRoundType)
            {
                case RoundType.OneEight:
                    indexes = oneEightIndexes;
                    break;
                case RoundType.QuarterFinal:
                    indexes = quarterFinalIndexes;
                    break;
                case RoundType.HalfFinal:
                    indexes = halfFinalIndexes;
                    break;
                default:
                    indexes = finalIndexes;
                    break;
            }
            if (fromRoundType == RoundType.Qualification)
            {
                return GetFromQual(source, indexes);
            }
            if (toRoundType == RoundType.Final)
            {
                return GetForFinal(source);
            }
            return GetFromPlayOff(source, indexes);
        }

        private IList<Athlete> GetFromQual(IList<Athlete> source, int[] indexes)
        {
            List<Athlete> athletes = source.Where(a => !a.IsDisqualified && !a.IsFallen).ToList();
            athletes.Sort();
            athletes = new List<Athlete>(athletes.Take(indexes.Length));
            List<Athlete> result = new List<Athlete>(indexes.Length);

            for (int i = 0; i < indexes.Length; i++)
            {
                result.Add(GetClearAthlete(athletes[indexes[i]]));
            }

            return result;
        }

        private IList<Athlete> GetFromPlayOff(IList<Athlete> source, int[] indexes)
        {
            List<Athlete> tmp = new List<Athlete>(source.Count / 2);

            for (int i = 0; i < source.Count / 2; i++)
            {
                tmp.Add(null);
            }

            for (int i= 0; i < tmp.Count; i++)
            {
                if (source[2*i].CompareTo(source[2*i + 1]) == -1)
                {
                    tmp[i] = source[2*i];
                }
                else
                {
                    tmp[i] = source[2 * i + 1];
                }
            }

            tmp.Sort();

            var result = new List<Athlete>();
            for (int i = 0; i < indexes.Length; i++)
            {
                result.Add(GetClearAthlete(tmp[indexes[i]]));
            }

            return result;
        }

        private IList<Athlete> GetForFinal(IList<Athlete> source)
        {
            List<Athlete> result = new List<Athlete>(source);
            if (source[0].CompareTo(source[1]) < 0)
            {
                result[2] = GetClearAthlete(source[0]);
                result[0] = GetClearAthlete(source[1]);
            }
            else
            {
                result[0] = GetClearAthlete(source[0]);
                result[2] = GetClearAthlete(source[1]);
            }

            if (source[2].CompareTo(source[3]) < 0)
            {
                result[3] = GetClearAthlete(source[2]);
                result[1] = GetClearAthlete(source[3]);
            }
            else
            {
                result[1] = GetClearAthlete(source[2]);
                result[3] = GetClearAthlete(source[3]);
            }

            return result;
        }

        private Athlete GetClearAthlete(Athlete athlete)
        {
            return new Athlete(athlete.AthleteID)
                       {
                           FIO = athlete.FIO
                       };
        }
    }
}

