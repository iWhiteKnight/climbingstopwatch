﻿using System;
namespace stopwatch_v1
{
    public interface IRoundProcessor
    {
        bool Transit();
        void StartRace();
        void Stop(int index, bool isFalseStart, bool isFall);
        void RestoreLastAthletes(Athlete firstAthlete, Athlete secondAthlete);
        Athlete FirstAthl { get; set; }
        Athlete SecAthl { get; set; }
        Round Round { get; }
        event EventHandler RaceFinished;
        event EventHandler<NumberEventArgs> FalseStartNotActive;
        event EventHandler<NumberEventArgs> FinishIsPressed;
        event AthleteEventHandler FalseStart;
        event AthleteEventHandler Disqualified;
        event EventHandler RaceStarted;
        event EventHandler<StopEventArgs> AthleteStopped;
    }

    public delegate void AthleteEventHandler(Athlete a);

    public class NumberEventArgs : EventArgs
    {
        public int Number { get; set; }
    }

    public class StopEventArgs : EventArgs
    {
        public int Index { get; set; }
        public bool IsFall { get; set; }
    }
}
