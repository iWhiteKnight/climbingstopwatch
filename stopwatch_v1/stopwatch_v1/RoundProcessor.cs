﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;
using System.Media;
using log4net;


namespace stopwatch_v1
{

    public class RoundProcessor : IRoundProcessor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (RoundProcessor));

        private readonly StopWatch _stopWatch;
        private readonly SoundPlayer _sndPlay;
        private readonly IAthletesProvider _athletesProvider;

        private bool _firstRunning;
        private bool _secondRunning;

        private object _syncLock = new object();

        private Random _random = new Random();
       

        //Race state context
        private bool _ready;
        private bool _started;
        private bool _stopped;
        //------------------

        public event EventHandler RaceFinished;
        public event AthleteEventHandler FalseStart;
        public event AthleteEventHandler Disqualified;
        public event EventHandler<NumberEventArgs> FalseStartNotActive;
        public event EventHandler<NumberEventArgs> FinishIsPressed;
        public event EventHandler RaceStarted;
        public event EventHandler<StopEventArgs> AthleteStopped;

        public Round Round { get; private set; }


        public void RestoreLastAthletes(Athlete firstAthlete, Athlete secondAthlete)
        {
            _athletesProvider.SetLastActivePair(firstAthlete, secondAthlete);
        }

        public Athlete FirstAthl { get; set; }
        
        public Athlete SecAthl { get; set; }


        public RoundProcessor(Round round, StopWatch stopWatch)
        {
            _stopWatch = stopWatch;
            _sndPlay = new SoundPlayer();

            Round = round;
            _athletesProvider = AthletesProviderFactory.GetAthletesProvider(round.Type);

        }

        public bool Transit()
        {
            lock (_syncLock)
            {
                var athlArray = _athletesProvider.SelectNextAthletes();
                FirstAthl = athlArray[0];
                SecAthl = athlArray[1];

                if (FirstAthl != null)
                {
                    FirstAthl.WasSelected = true;
                }
                if (SecAthl != null)
                {
                    SecAthl.WasSelected = true;
                }

                _firstRunning = athlArray[0] != null;
                _secondRunning = athlArray[1] != null;
            }

            _log.InfoFormat("На старте: 1. {0}, 2. {1}", FirstAthl != null ? FirstAthl.FIO : "null", SecAthl != null ? SecAthl.FIO : "null");

            return _firstRunning || _secondRunning;
        }

        public void StartRace()
        {
            _log.Info("Начало забега");
            try
            {
                if (!String.IsNullOrEmpty(EquipmentController.Instance.GetPortName(1)))
                {
                    EquipmentController.Instance.OpenPort(1);
                    COM_data.com_n1.PinChanged += ComN1OnPinChanged;
                }
                if (!String.IsNullOrEmpty(EquipmentController.Instance.GetPortName(2)))
                {
                    EquipmentController.Instance.OpenPort(2);
                    COM_data.com_n2.PinChanged += ComN2OnPinChanged;
                }


                if (_firstRunning && !COM_data.com_n1.DsrHolding)
                {
                    FalseStartNotActive(this, new NumberEventArgs{ Number = 1 });
                    COM_data.com_n1.PinChanged -= ComN1OnPinChanged;
                    COM_data.com_n2.PinChanged -= ComN2OnPinChanged;
                    return;
                }
                if (_secondRunning && !COM_data.com_n2.DsrHolding)
                {
                    FalseStartNotActive(this, new NumberEventArgs { Number = 2});
                    COM_data.com_n1.PinChanged -= ComN1OnPinChanged;
                    COM_data.com_n2.PinChanged -= ComN2OnPinChanged;
                    return;
                }
                if (COM_data.com_n1.CtsHolding)
                {
                    FinishIsPressed(this, new NumberEventArgs {Number = 1});
                    COM_data.com_n1.PinChanged -= ComN1OnPinChanged;
                    COM_data.com_n2.PinChanged -= ComN2OnPinChanged;
                    return;
                }
                if (COM_data.com_n2.CtsHolding)
                {
                    FinishIsPressed(this, new NumberEventArgs { Number = 2 });
                    COM_data.com_n1.PinChanged -= ComN1OnPinChanged;
                    COM_data.com_n2.PinChanged -= ComN2OnPinChanged;
                    return;
                }

                _stopped = false;
                _sndPlay.Stream = Properties.Resources.readyBeep;
                _sndPlay.Play();
                _ready = true;

                Thread waitThread = new Thread(WaitStart);
                waitThread.Start();

            }
            catch (UnauthorizedAccessException)
            {
                EquipmentController.Instance.ClosePort(1);
                EquipmentController.Instance.ClosePort(2);
                _log.Error("Один из портов занят");
                MessageBox.Show("Один из портов занят. Освободите порт и попробуйте снова");
            }
        }

        private void WaitStart(object param)
        {
            Thread.Sleep(_random.Next(2000, 3000));

            bool stopped;
            lock (_syncLock)
            {
                stopped = _stopped;
            }

            if (!stopped)
            {
                _sndPlay.Stream = Properties.Resources.startBeep;
                _started = true;
                _stopWatch.Start(_firstRunning, _secondRunning);
                _sndPlay.Play();

                _log.Info("Старт!");

                if (RaceStarted != null)
                {
                    RaceStarted(this, EventArgs.Empty);
                }
            }
        }

        private void ComN1OnPinChanged(object sender, SerialPinChangedEventArgs eventArgs)
        {
            _log.InfoFormat("Port 1 PinChanged:  EventType: {0}; CtsHolding: {1}; DsrHolding: {2}",
                eventArgs.EventType, COM_data.com_n1.CtsHolding, COM_data.com_n1.DsrHolding);
            PinChanged((SerialPort)sender, eventArgs, 1);
        }

        private void ComN2OnPinChanged(object sender, SerialPinChangedEventArgs eventArgs)
        {
            _log.InfoFormat("Port 2 PinChanged:  EventType: {0}; CtsHolding: {1}; DsrHolding: {2}",
                eventArgs.EventType, COM_data.com_n2.CtsHolding, COM_data.com_n2.DsrHolding);
            PinChanged((SerialPort)sender, eventArgs, 2);
        }

        private void PinChanged(SerialPort comPort, SerialPinChangedEventArgs eventArgs, int index)
        {
            switch (index)
            {
                case 1:
                    if (_ready && _firstRunning) //waiting for start and control falseStart
                    {

                        if (eventArgs.EventType == SerialPinChange.DsrChanged && !_started)
                        {
                            //Фальстарт если раунд не начался
                            lock (_syncLock)
                            {
                                _sndPlay.Stream = Properties.Resources.falseStartBeep;
                                _sndPlay.Play();
                                if (!FirstAthl.ResultT1Cl.FalseStart)
                                {
                                    FirstAthl.ResultT1Cl.FalseStart = true;
                                    FalseStart(FirstAthl);
                                }
                                else
                                {
                                    FirstAthl.ResultT1Cl.Disqualified = true;
                                    Disqualified(FirstAthl);
                                    _log.InfoFormat("{0} дисквалифицирован", FirstAthl.FIO);
                                }
                                _stopped = true;
                            }
                            Stop(index, true, false);
                        }
                        else if (eventArgs.EventType == SerialPinChange.CtsChanged)
                        {
                            Stop(index, false, false);
                        }
                    }

                    break;
                case 2:
                    if (_ready && _secondRunning) //waiting for start and control falseStart
                    {
                        if (eventArgs.EventType == SerialPinChange.DsrChanged && !_started)
                        {
                            //Фальстарт если раунд не начался
                            lock (_syncLock)
                            {
                                _sndPlay.Stream = Properties.Resources.falseStartBeep;
                                _sndPlay.Play();
                                if (!SecAthl.ResultT2Cl.FalseStart)
                                {
                                    SecAthl.ResultT2Cl.FalseStart = true;
                                    FalseStart(SecAthl);
                                }
                                else
                                {
                                    SecAthl.ResultT2Cl.Disqualified = true;
                                    Disqualified(SecAthl);
                                    _log.InfoFormat("{0} дисквалифицирован", SecAthl.FIO);
                                }
                                _stopped = true;
                            }
                            Stop(index, true, false);
                        }
                        else if (eventArgs.EventType == SerialPinChange.CtsChanged)
                        {
                            Stop(index, false, false);
                        }
                    }

                    break;
            }
        }

        public void Stop(int index, bool isFalseStart, bool isFall)
        {
            var elapsed = _stopWatch.Stop(index);

            if (isFalseStart)
            {
                _stopWatch.Reset();
                _started = false;
                _ready = false;

                EquipmentController.Instance.ClosePort(1);
                EquipmentController.Instance.ClosePort(2);

                _log.InfoFormat("Спортсмен {0} совершил фальстарт.", index);
                return;
            }
            UpdateAthlete(index, elapsed, isFall);
            switch (index)
            {
                case 1:
                    {
                        if (COM_data.com_n1 != null)
                        {
                            COM_data.com_n1.PinChanged -= ComN1OnPinChanged;
                        }
                        if (SecAthl == null || SecAthl.ResultT2Cl.ResultTime > TimeSpan.Zero || SecAthl.ResultT2Cl.Fell)
                            FinishRace();
                    }
                    break;
                case 2:
                    {
                        if (COM_data.com_n2 != null)
                        {
                            COM_data.com_n2.PinChanged -= ComN2OnPinChanged;
                        }
                        if (FirstAthl == null || FirstAthl.ResultT1Cl.ResultTime > TimeSpan.Zero || FirstAthl.ResultT1Cl.Fell)
                            FinishRace();
                    }
                    break;
            }
            EquipmentController.Instance.ClosePort(index);

            if (AthleteStopped != null)
            {
                AthleteStopped(this, new StopEventArgs
                                         {
                                             Index = index,
                                             IsFall = isFall
                                         });
            }
        }

        private void FinishRace()
        {
            _log.Info("Окончание забега");

            if (COM_data.com_n1 != null)
            {
                COM_data.com_n1.PinChanged -= ComN1OnPinChanged;
                EquipmentController.Instance.ClosePort(1);
            }
            if (COM_data.com_n2 != null)
            {
                COM_data.com_n2.PinChanged -= ComN2OnPinChanged;
                EquipmentController.Instance.ClosePort(2);
            }

            _stopWatch.Reset();

            RaceFinished(null, EventArgs.Empty);
            _started = false;
            _stopped = false;
            _ready = false;
        }

        private void UpdateAthlete(int index, TimeSpan elapsed, bool isFall)
        {
            if (FirstAthl != null || SecAthl != null)
                lock (_syncLock)
                {
                    switch (index)
                    {
                        case 1:
                            FirstAthl.ResultT1Cl.Fell = isFall;
                            if (FirstAthl.ResultT1Cl.Fell || FirstAthl.IsDisqualified)
                                FirstAthl.ResultT1Cl.ResultTime = TimeSpan.Zero;
                            else
                                FirstAthl.ResultT1Cl.ResultTime = elapsed;


                            break;
                        case 2:
                            SecAthl.ResultT2Cl.Fell = isFall;
                            if (SecAthl.ResultT2Cl.Fell || SecAthl.IsDisqualified)
                                SecAthl.ResultT2Cl.ResultTime = TimeSpan.Zero;
                            else
                                SecAthl.ResultT2Cl.ResultTime = elapsed;


                            break;
                        default:
                            throw new ArgumentException("Athlete number must be 1 or 2");
                    }
                }
            if (isFall)
            {
                _log.InfoFormat("Спортсмен {0} сорвался", index);
                
            }
            else
            {
                _log.InfoFormat("Спортсмен {0} пробежал за {1}", index, elapsed);                
            }
        }
    }
}
