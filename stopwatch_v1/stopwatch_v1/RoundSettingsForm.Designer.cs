﻿namespace stopwatch_v1
{
    partial class RoundSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonClassic = new System.Windows.Forms.RadioButton();
            this.radioButtonIdent = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonFinal = new System.Windows.Forms.RadioButton();
            this.radioButtonHalf = new System.Windows.Forms.RadioButton();
            this.radioButtonForth = new System.Windows.Forms.RadioButton();
            this.radioButtonEighth = new System.Windows.Forms.RadioButton();
            this.radioButtonQual = new System.Windows.Forms.RadioButton();
            this.button_begin = new System.Windows.Forms.Button();
            this.button_exit = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonMale = new System.Windows.Forms.RadioButton();
            this.radioButtonFemale = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonClassic
            // 
            this.radioButtonClassic.AutoSize = true;
            this.radioButtonClassic.Checked = true;
            this.radioButtonClassic.Enabled = false;
            this.radioButtonClassic.Location = new System.Drawing.Point(16, 19);
            this.radioButtonClassic.Name = "radioButtonClassic";
            this.radioButtonClassic.Size = new System.Drawing.Size(97, 17);
            this.radioButtonClassic.TabIndex = 0;
            this.radioButtonClassic.TabStop = true;
            this.radioButtonClassic.Text = "Классический";
            this.radioButtonClassic.UseVisualStyleBackColor = true;
            // 
            // radioButtonIdent
            // 
            this.radioButtonIdent.AutoSize = true;
            this.radioButtonIdent.Enabled = false;
            this.radioButtonIdent.Location = new System.Drawing.Point(16, 42);
            this.radioButtonIdent.Name = "radioButtonIdent";
            this.radioButtonIdent.Size = new System.Drawing.Size(127, 17);
            this.radioButtonIdent.TabIndex = 0;
            this.radioButtonIdent.TabStop = true;
            this.radioButtonIdent.Text = "Идентичные трассы";
            this.radioButtonIdent.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonClassic);
            this.groupBox1.Controls.Add(this.radioButtonIdent);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(151, 69);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Формат соревнований";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonFinal);
            this.groupBox2.Controls.Add(this.radioButtonHalf);
            this.groupBox2.Controls.Add(this.radioButtonForth);
            this.groupBox2.Controls.Add(this.radioButtonEighth);
            this.groupBox2.Controls.Add(this.radioButtonQual);
            this.groupBox2.Location = new System.Drawing.Point(185, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(144, 145);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Этап соревнований";
            // 
            // radioButtonFinal
            // 
            this.radioButtonFinal.AutoSize = true;
            this.radioButtonFinal.Location = new System.Drawing.Point(16, 111);
            this.radioButtonFinal.Name = "radioButtonFinal";
            this.radioButtonFinal.Size = new System.Drawing.Size(60, 17);
            this.radioButtonFinal.TabIndex = 0;
            this.radioButtonFinal.Text = "Финал";
            this.radioButtonFinal.UseVisualStyleBackColor = true;
            // 
            // radioButtonHalf
            // 
            this.radioButtonHalf.AutoSize = true;
            this.radioButtonHalf.Location = new System.Drawing.Point(16, 88);
            this.radioButtonHalf.Name = "radioButtonHalf";
            this.radioButtonHalf.Size = new System.Drawing.Size(83, 17);
            this.radioButtonHalf.TabIndex = 0;
            this.radioButtonHalf.Text = "1/2 финала";
            this.radioButtonHalf.UseVisualStyleBackColor = true;
            // 
            // radioButtonForth
            // 
            this.radioButtonForth.AutoSize = true;
            this.radioButtonForth.Location = new System.Drawing.Point(16, 65);
            this.radioButtonForth.Name = "radioButtonForth";
            this.radioButtonForth.Size = new System.Drawing.Size(83, 17);
            this.radioButtonForth.TabIndex = 0;
            this.radioButtonForth.Text = "1/4 финала";
            this.radioButtonForth.UseVisualStyleBackColor = true;
            // 
            // radioButtonEighth
            // 
            this.radioButtonEighth.AutoSize = true;
            this.radioButtonEighth.Location = new System.Drawing.Point(16, 42);
            this.radioButtonEighth.Name = "radioButtonEighth";
            this.radioButtonEighth.Size = new System.Drawing.Size(83, 17);
            this.radioButtonEighth.TabIndex = 0;
            this.radioButtonEighth.Text = "1/8 финала";
            this.radioButtonEighth.UseVisualStyleBackColor = true;
            // 
            // radioButtonQual
            // 
            this.radioButtonQual.AutoSize = true;
            this.radioButtonQual.Checked = true;
            this.radioButtonQual.Location = new System.Drawing.Point(16, 19);
            this.radioButtonQual.Name = "radioButtonQual";
            this.radioButtonQual.Size = new System.Drawing.Size(100, 17);
            this.radioButtonQual.TabIndex = 0;
            this.radioButtonQual.TabStop = true;
            this.radioButtonQual.Text = "Квалификация";
            this.radioButtonQual.UseVisualStyleBackColor = true;
            // 
            // button_begin
            // 
            this.button_begin.Location = new System.Drawing.Point(12, 170);
            this.button_begin.Name = "button_begin";
            this.button_begin.Size = new System.Drawing.Size(127, 23);
            this.button_begin.TabIndex = 2;
            this.button_begin.Text = "Начать";
            this.button_begin.UseVisualStyleBackColor = true;
            this.button_begin.Click += new System.EventHandler(this.button_begin_Click);
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(200, 170);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(127, 23);
            this.button_exit.TabIndex = 2;
            this.button_exit.Text = "Выход";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonMale);
            this.groupBox3.Controls.Add(this.radioButtonFemale);
            this.groupBox3.Location = new System.Drawing.Point(12, 87);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(151, 69);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Зачет";
            // 
            // radioButtonMale
            // 
            this.radioButtonMale.AutoSize = true;
            this.radioButtonMale.Checked = true;
            this.radioButtonMale.Location = new System.Drawing.Point(16, 19);
            this.radioButtonMale.Name = "radioButtonMale";
            this.radioButtonMale.Size = new System.Drawing.Size(71, 17);
            this.radioButtonMale.TabIndex = 0;
            this.radioButtonMale.TabStop = true;
            this.radioButtonMale.Text = "Мужской";
            this.radioButtonMale.UseVisualStyleBackColor = true;
            // 
            // radioButtonFemale
            // 
            this.radioButtonFemale.AutoSize = true;
            this.radioButtonFemale.Location = new System.Drawing.Point(16, 42);
            this.radioButtonFemale.Name = "radioButtonFemale";
            this.radioButtonFemale.Size = new System.Drawing.Size(72, 17);
            this.radioButtonFemale.TabIndex = 0;
            this.radioButtonFemale.TabStop = true;
            this.radioButtonFemale.Text = "Женский";
            this.radioButtonFemale.UseVisualStyleBackColor = true;
            // 
            // RoundSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 205);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.button_begin);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RoundSettingsForm";
            this.Text = "Установки";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonClassic;
        private System.Windows.Forms.RadioButton radioButtonIdent;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonForth;
        private System.Windows.Forms.RadioButton radioButtonEighth;
        private System.Windows.Forms.RadioButton radioButtonQual;
        private System.Windows.Forms.RadioButton radioButtonFinal;
        private System.Windows.Forms.RadioButton radioButtonHalf;
        private System.Windows.Forms.Button button_begin;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonMale;
        private System.Windows.Forms.RadioButton radioButtonFemale;

    }
}