﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;
using System.Threading;

namespace stopwatch_v1
{
    public partial class EquipmentSettings : Form
    {
        private String[] com_port_names = SerialPort.GetPortNames();
        //private static String[] com_port_names = { "COM0", "COM1", "COM2" };

        public EquipmentSettings()
        {
            InitializeComponent();

            // Инициализация COM-портов
            if (com_port_names.Length == 0)
            {
                MessageBox.Show("Нет ни одного COM-порта");
                combo_com_n1.Enabled = false;
                button_test_n1.Enabled = false;
                combo_com_n2.Enabled = false;
                button_test_n2.Enabled = false;
            }
            else
            {
                combo_com_n1.Items.Add("");
                for (int i = 0; i < com_port_names.Length; i++)
                    combo_com_n1.Items.Add(com_port_names[i]);

                combo_com_n2.Items.Add("");
                for (int i = 0; i < com_port_names.Length; i++)
                    combo_com_n2.Items.Add(com_port_names[i]);


                // Подгрузка данных из структуры COM-портов
                load_data(COM_data.name_n1, combo_com_n1, button_test_n1);
                load_data(COM_data.name_n2, combo_com_n2, button_test_n2);
            }
        }

        private void load_data(String com_name, ComboBox comBox, Button btTest)
        {
            comBox.SelectedIndex = find_COM_idx(com_name);

            if(String.IsNullOrEmpty(comBox.Text))
            {
                btTest.Enabled = false;
            }
            else
            {
                btTest.Enabled = true;
                
            }
        }

        private int find_COM_idx(String com_n)
        {
            for (int i = 0; i < combo_com_n1.Items.Count; i++)
                if (combo_com_n1.Items[i].Equals(com_n))
                        return i;
            return -1;
        }

        private void button_test_n1_Click(object sender, EventArgs e)
        {
            Form_test F_test = new Form_test(1, combo_com_n1.Text);
            try
            {
                F_test.ShowDialog();
            }
            catch (System.IO.IOException)
            { }
        }

        private void button_test_n2_Click(object sender, EventArgs e)
        {
            Form_test F_test = new Form_test(2, combo_com_n2.Text);
            try
            {
                F_test.ShowDialog();
            }
            catch (System.IO.IOException)
            { }
        }

        private void button_accept_Click(object sender, EventArgs e)
        {
            // Проверка на выбор одного и того же порта для обеих трасс
            if (!String.IsNullOrEmpty(combo_com_n1.Text) 
                && !String.IsNullOrEmpty(combo_com_n2.Text) 
                && combo_com_n1.SelectedIndex == combo_com_n2.SelectedIndex)
            {
                if (com_port_names.Length == 1)
                    MessageBox.Show("Отключите одну трассу");
                else
                    MessageBox.Show("Выбран один и тот же порт для двух трасс");
            }
            else
            {

                COM_data.name_n1 = combo_com_n1.Text;
                COM_data.name_n2 = combo_com_n2.Text;

                Properties.Settings.Default.Com1 = combo_com_n1.Text;
                Properties.Settings.Default.Com2 = combo_com_n2.Text;
                Properties.Settings.Default.Save();

                // Закрытие формы
                Close();
            }
        }

        private void portChanged(ComboBox comBox, Button btTest)
        {
            if (com_port_names.Length == 0 || String.IsNullOrEmpty(comBox.Text))
            {
                btTest.Enabled = false;
            }
            else
            {
                btTest.Enabled = true;
            }
        }


        private void button_discard_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void combo_com_n1_SelectedIndexChanged(object sender, EventArgs e)
        {
            portChanged(combo_com_n1, button_test_n1);
        }

        private void combo_com_n2_SelectedIndexChanged(object sender, EventArgs e)
        {
            portChanged(combo_com_n2, button_test_n2);
        }
    }
}
