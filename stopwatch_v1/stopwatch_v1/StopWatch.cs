﻿using System;
using System.Diagnostics;
using System.Threading;
using log4net;

namespace stopwatch_v1
{
    public class StopWatch
    {
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private Thread _resultThread;

        private readonly IDisplayable _displayable1;
        private readonly IDisplayable _displayable2;

        private static readonly ILog _logger = LogManager.GetLogger(typeof(StopWatch));
        
        private readonly object _syncLock = new object();

        private bool _doRun1;
        private bool _doRun2;

        public StopWatch(IDisplayable displayable1, IDisplayable displayable2)
        {
            _displayable1 = new DisplayableNullProxy(displayable1);
            _displayable2 = new DisplayableNullProxy(displayable2);
        }

        public void Start(bool doRun1, bool doRun2)
        {
            _doRun1 = doRun1;
            _doRun2 = doRun2; 
            _resultThread = new Thread(ResultUpdate);
            _stopwatch.Reset();
            _stopwatch.Start();
            _resultThread.Start();
        }

        public void ResultUpdate(object param)
        {
            while (true)
            {
                bool doRun1;
                bool doRun2;
                lock (_syncLock)
                {
                    doRun1 = _doRun1;
                    doRun2 = _doRun2;
                }

                if (!doRun1 && !doRun2)
                {
                    _stopwatch.Stop();
                    break;
                }

                string elapsed = _stopwatch.Elapsed.FormatTimeSpan();
                try
                {
                    if (doRun1)
                    {
                        _displayable1.Display(elapsed);
                    }
                    if (doRun2)
                    {
                        _displayable2.Display(elapsed);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Error when running stopwatch", ex);
                }

                Thread.Sleep(100);

            }
        }

        public TimeSpan Stop(int index)
        {
            TimeSpan result = _stopwatch.Elapsed;
            string resultString = _stopwatch.Elapsed.FormatTimeSpan();
            lock (_syncLock)
            {
                switch (index)
                {
                    case 1:
                        _doRun1 = false;
                        _displayable1.Display(resultString);
                        break;
                    case 2:
                        _doRun2 = false;
                        _displayable2.Display(resultString);
                        break;
                }
            }
            return result;
        }

        public void Reset()
        {
            _stopwatch.Reset();
        }

    }
}