﻿using System;

namespace stopwatch_v1
{
    public class Round
    {
        public Round(RoundType type, bool male)
        {
            Date = DateTime.Now;
            Type = type;
            Male = male;
        }

        public DateTime Date { get; set; }

        public RoundType Type { get; set; }

        public bool Male { get; set; }

        public override string ToString()
        {
            var genderString = Male ? "мужчины" : "женщины";
            return string.Format("Скорость_{0}_{1}_{2}", Type.GetString(), genderString, Date.ToShortDateString());
        }
    }
}
