﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stopwatch_v1
{
    public class FinalAthletesProvider : IAthletesProvider
    {
        private IList<Athlete> _list;
        private Athlete _firstAthlete;
        private Athlete _secAthlete;
        private int _disqualIndex = -1;


        public FinalAthletesProvider(IList<Athlete> list)
        {
            _list = list;
        }

        public Athlete[] SelectNextAthletes()
        {
            Athlete[] athlArray = new Athlete[2];
            if (_list.Count > 1)
            {
                if (_firstAthlete == null && _secAthlete == null)//beginning of round
                {
                    athlArray[0] = _list[0];
                    athlArray[1] = _list[1];
                }
                else if (_firstAthlete != null && _secAthlete != null)//common case
                {
                    var fi = _list.IndexOf(_firstAthlete);
                    var si = _list.IndexOf(_secAthlete);
                    var maxi = SelectMaxIndex(fi, si);


                    if (_firstAthlete.FinishedRound || _secAthlete.FinishedRound)
                    {
                        SetNewPair(athlArray, maxi);
                        _disqualIndex = -1;
                    }
                    else
                    {
                        if (!_firstAthlete.IsDisqualified && !_secAthlete.IsDisqualified)//both OK
                        {
                            SwapAthl(athlArray);
                            _disqualIndex = -1;
                        }
                        else if (!_firstAthlete.IsDisqualified && _secAthlete.IsDisqualified)//second disqual
                        {
                            athlArray[0] = _firstAthlete;
                            _disqualIndex = maxi;
                        }
                        else //first disqual
                        {
                            athlArray[1] = _secAthlete;
                            _disqualIndex = maxi;
                        }
                    }
                }
                else if (_firstAthlete != null)//second disqulified or list ended
                {
                    if (!_firstAthlete.FinishedRound && !_firstAthlete.IsDisqualified)
                    {
                        athlArray[1] = _firstAthlete;
                        //_disqualIndex = -1;
                    }

                    else if (_disqualIndex > -1)
                    {
                        SetNewPair(athlArray, _disqualIndex);
                        _disqualIndex = -1;
                    }
                    else
                    {
                        _disqualIndex = -1;
                    }
                }
                else//first disqulified or list ended
                {
                    if (!_secAthlete.FinishedRound && !_secAthlete.IsDisqualified)
                    {
                        athlArray[0] = _secAthlete;
                        //_disqualIndex = -1;
                    }

                    else if (_disqualIndex > -1)
                    {
                        SetNewPair(athlArray, _disqualIndex);
                        _disqualIndex = -1;
                    }
                    else
                    {
                        _disqualIndex = -1;
                    }

                }
                _firstAthlete = athlArray[0];
                _secAthlete = athlArray[1];

                return athlArray;
            }

            throw new InvalidOperationException("Athletes list is empty");
        }

        public void SetLastActivePair(Athlete firstAthlete, Athlete secondAthlete)
        {
            _firstAthlete = firstAthlete;
            _secAthlete = secondAthlete;
        }

        private int SelectMaxIndex(int i, int j)
        {
            if (i == j) throw new InvalidOperationException();
            if (i > j) return i;
            else return j;
        }

        private void SetNewPair(Athlete[] athlArray, int maxi)
        {
            if (!IsLast(maxi))
                athlArray[0] = _list[maxi + 1];

            if (!IsLast(maxi + 1))
                athlArray[1] = _list[maxi + 2];

            _disqualIndex = -1;
        }

        private Athlete[] SwapAthl(Athlete[] athlArray)
        {
            athlArray[0] = _secAthlete;
            athlArray[1] = _firstAthlete;
            return athlArray;
        }

        private bool IsLast(int index)
        {
            if (_list.Count <= index + 1)
                return true;
            return false;
        }
    }
}
