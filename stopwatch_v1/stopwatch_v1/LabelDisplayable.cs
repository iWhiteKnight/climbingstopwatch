﻿using System.Windows.Forms;

namespace stopwatch_v1
{
    internal class LabelDisplayable : IDisplayable
    {
        private readonly Label _label;

        public LabelDisplayable(Label label)
        {
            _label = label;
        }

        public void Display(string text)
        {
            _label.Invoke(new MethodInvoker(delegate { _label.Text = text; }));
        }
    }
}
