﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;
using System.Threading;

namespace test_com0com_sharp
{
    public partial class Form1 : Form
    {
        private String[] com_port_names = SerialPort.GetPortNames();
        static SerialPort com_n1;
        static SerialPort com_n2;
        static bool start = false;
        static bool finish = false;

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < com_port_names.Length; i++)
                combo_com_n1.Items.Add(com_port_names[i]);
            for (int i = 0; i < com_port_names.Length; i++)
                combo_com_n2.Items.Add(com_port_names[i]);

            combo_com_n1.SelectedIndex = 0;
            combo_com_n2.SelectedIndex = 1;

            button_close1.Enabled = false;
            button_close2.Enabled = false;
            button_start.BackColor = SystemColors.InactiveBorder;
            button_finish.BackColor = SystemColors.InactiveBorder;
            state_start.Enabled = false;
            state_start.BackColor = SystemColors.InactiveBorder;
            state_finish.Enabled = false;
            state_finish.BackColor = SystemColors.InactiveBorder;

        }

        private void button_open1_Click(object sender, EventArgs e)
        {
            // Open COM port
            if (com_n1 == null)
            {
                com_n1 = new SerialPort(combo_com_n1.Text);
                com_n1.Handshake = Handshake.XOnXOff;
                com_n1.DtrEnable = false;
                com_n1.RtsEnable = false;
                try
                {
                    com_n1.Open();
                }
                catch (System.IO.IOException)
                {
                    com_n1 = null;
                    MessageBox.Show("Не удается открыть COM-порт");
                }
                if (com_n1 != null)
                {
                    button_open1.Enabled = false;
                    button_close1.Enabled = true;
                }
            }
        }

        private void button_open2_Click(object sender, EventArgs e)
        {
            // Open COM port
            if (com_n2 == null)
            {
                com_n2 = new SerialPort(combo_com_n2.Text);
                com_n2.Handshake = Handshake.RequestToSendXOnXOff;
                com_n2.DtrEnable = false;
                com_n2.RtsEnable = false;
                try
                {
                    com_n2.Open();
                }
                catch (System.IO.IOException)
                {
                    com_n2 = null;
                    MessageBox.Show("Не удается открыть COM-порт");
                }
                if (com_n2 != null)
                {
                    button_open2.Enabled = false;
                    button_close2.Enabled = true;
                    com_n2.PinChanged += new SerialPinChangedEventHandler(com_event);
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (com_n1 != null)
            {
                if (com_n1.IsOpen)
                    com_n1.Close();
                com_n1 = null;
            }

            if (com_n2 != null)
            {
                if (com_n2.IsOpen)
                    com_n2.Close();
                com_n2 = null;
            }
        }

        private void button_close1_Click(object sender, EventArgs e)
        {
            if (com_n1 != null)
            {
                if (com_n1.IsOpen)
                    com_n1.Close();
                com_n1 = null;
            }
            button_open1.Enabled = true;
            button_close1.Enabled = false;
        }


        private void button_close2_Click(object sender, EventArgs e)
        {
            if (com_n2 != null)
            {
                if (com_n2.IsOpen)
                    com_n2.Close();
                com_n2 = null;
            }
            button_open2.Enabled = true;
            button_close2.Enabled = false;
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            if (com_n1 != null)
            {
                if (start == false)
                {
                    start = true;
                    com_n1.RtsEnable = true;
                    button_start.BackColor = SystemColors.HotTrack;
                }
                else
                {
                    start = false;
                    com_n1.RtsEnable = false;
                    button_start.BackColor = SystemColors.InactiveBorder;
                }
            }
            else
                MessageBox.Show("Порт закрыт");

        }

        private void button_finish_Click(object sender, EventArgs e)
        {
            if (com_n1 != null)
            {
                if (finish == false)
                {
                    finish = true;
                    com_n1.DtrEnable = true;
                    button_finish.BackColor = SystemColors.HotTrack;
                }
                else
                {
                    finish = false;
                    com_n1.DtrEnable = false;
                    button_finish.BackColor = SystemColors.InactiveBorder;
                }
            }
            else
                MessageBox.Show("Порт закрыт");
            
        }

        private void com_event(object sender, SerialPinChangedEventArgs event_args)
        {
            if (event_args.EventType == SerialPinChange.CtsChanged)
            {
                if (com_n2.CtsHolding == true)
                    state_start.BackColor = SystemColors.HotTrack;
                else
                    state_start.BackColor = SystemColors.InactiveBorder;

            }
            else if (event_args.EventType == SerialPinChange.DsrChanged)
            {
                if (com_n2.DsrHolding == true)
                    state_finish.BackColor = SystemColors.HotTrack;
                else
                    state_finish.BackColor = SystemColors.InactiveBorder;
            }
        }

    }
}
