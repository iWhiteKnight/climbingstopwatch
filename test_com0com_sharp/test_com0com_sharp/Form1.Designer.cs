﻿namespace test_com0com_sharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_start = new System.Windows.Forms.Button();
            this.button_finish = new System.Windows.Forms.Button();
            this.state_start = new System.Windows.Forms.Button();
            this.state_finish = new System.Windows.Forms.Button();
            this.combo_com_n1 = new System.Windows.Forms.ComboBox();
            this.label_com1 = new System.Windows.Forms.Label();
            this.combo_com_n2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_open1 = new System.Windows.Forms.Button();
            this.button_open2 = new System.Windows.Forms.Button();
            this.button_close1 = new System.Windows.Forms.Button();
            this.button_close2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(35, 124);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(71, 74);
            this.button_start.TabIndex = 0;
            this.button_start.Text = "Start";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button_finish
            // 
            this.button_finish.Location = new System.Drawing.Point(115, 124);
            this.button_finish.Name = "button_finish";
            this.button_finish.Size = new System.Drawing.Size(71, 74);
            this.button_finish.TabIndex = 0;
            this.button_finish.Text = "Finish";
            this.button_finish.UseVisualStyleBackColor = true;
            this.button_finish.Click += new System.EventHandler(this.button_finish_Click);
            // 
            // state_start
            // 
            this.state_start.Location = new System.Drawing.Point(286, 124);
            this.state_start.Name = "state_start";
            this.state_start.Size = new System.Drawing.Size(71, 74);
            this.state_start.TabIndex = 0;
            this.state_start.Text = "Start";
            this.state_start.UseVisualStyleBackColor = true;
            // 
            // state_finish
            // 
            this.state_finish.Location = new System.Drawing.Point(366, 124);
            this.state_finish.Name = "state_finish";
            this.state_finish.Size = new System.Drawing.Size(71, 74);
            this.state_finish.TabIndex = 0;
            this.state_finish.Text = "Finish";
            this.state_finish.UseVisualStyleBackColor = true;
            // 
            // combo_com_n1
            // 
            this.combo_com_n1.FormattingEnabled = true;
            this.combo_com_n1.Location = new System.Drawing.Point(80, 31);
            this.combo_com_n1.Name = "combo_com_n1";
            this.combo_com_n1.Size = new System.Drawing.Size(121, 21);
            this.combo_com_n1.TabIndex = 1;
            // 
            // label_com1
            // 
            this.label_com1.AutoSize = true;
            this.label_com1.Location = new System.Drawing.Point(16, 34);
            this.label_com1.Name = "label_com1";
            this.label_com1.Size = new System.Drawing.Size(60, 13);
            this.label_com1.TabIndex = 2;
            this.label_com1.Text = "COM-порт:";
            // 
            // combo_com_n2
            // 
            this.combo_com_n2.FormattingEnabled = true;
            this.combo_com_n2.Location = new System.Drawing.Point(327, 31);
            this.combo_com_n2.Name = "combo_com_n2";
            this.combo_com_n2.Size = new System.Drawing.Size(121, 21);
            this.combo_com_n2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(263, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "COM-порт:";
            // 
            // button_open1
            // 
            this.button_open1.Location = new System.Drawing.Point(35, 58);
            this.button_open1.Name = "button_open1";
            this.button_open1.Size = new System.Drawing.Size(151, 23);
            this.button_open1.TabIndex = 3;
            this.button_open1.Text = "Открыть порт";
            this.button_open1.UseVisualStyleBackColor = true;
            this.button_open1.Click += new System.EventHandler(this.button_open1_Click);
            // 
            // button_open2
            // 
            this.button_open2.Location = new System.Drawing.Point(286, 58);
            this.button_open2.Name = "button_open2";
            this.button_open2.Size = new System.Drawing.Size(151, 23);
            this.button_open2.TabIndex = 3;
            this.button_open2.Text = "Открыть порт";
            this.button_open2.UseVisualStyleBackColor = true;
            this.button_open2.Click += new System.EventHandler(this.button_open2_Click);
            // 
            // button_close1
            // 
            this.button_close1.Location = new System.Drawing.Point(35, 87);
            this.button_close1.Name = "button_close1";
            this.button_close1.Size = new System.Drawing.Size(151, 23);
            this.button_close1.TabIndex = 3;
            this.button_close1.Text = "Закрыть порт";
            this.button_close1.UseVisualStyleBackColor = true;
            this.button_close1.Click += new System.EventHandler(this.button_close1_Click);
            // 
            // button_close2
            // 
            this.button_close2.Location = new System.Drawing.Point(286, 87);
            this.button_close2.Name = "button_close2";
            this.button_close2.Size = new System.Drawing.Size(151, 23);
            this.button_close2.TabIndex = 3;
            this.button_close2.Text = "Закрыть порт";
            this.button_close2.UseVisualStyleBackColor = true;
            this.button_close2.Click += new System.EventHandler(this.button_close2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 213);
            this.Controls.Add(this.button_close2);
            this.Controls.Add(this.button_open2);
            this.Controls.Add(this.button_close1);
            this.Controls.Add(this.button_open1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_com1);
            this.Controls.Add(this.combo_com_n2);
            this.Controls.Add(this.combo_com_n1);
            this.Controls.Add(this.state_finish);
            this.Controls.Add(this.button_finish);
            this.Controls.Add(this.state_start);
            this.Controls.Add(this.button_start);
            this.Name = "Form1";
            this.Text = "Тест";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_finish;
        private System.Windows.Forms.Button state_start;
        private System.Windows.Forms.Button state_finish;
        private System.Windows.Forms.ComboBox combo_com_n1;
        private System.Windows.Forms.Label label_com1;
        private System.Windows.Forms.ComboBox combo_com_n2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_open1;
        private System.Windows.Forms.Button button_open2;
        private System.Windows.Forms.Button button_close1;
        private System.Windows.Forms.Button button_close2;
    }
}

