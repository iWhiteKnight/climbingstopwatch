﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace text_xls_sharp_v1
{
    public partial class Form1 : Form
    {
        Microsoft.Office.Interop.Excel.Application excelapp;
        
        Workbook wkbook;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_open_Click(object sender, EventArgs e)
        {
            excelapp = new Microsoft.Office.Interop.Excel.Application();
            String fileName;

            // Open file
            openFileDialog1.InitialDirectory = "f:\\Climbing\\Stopwatch\\VC\\text_xls_sharp_v1\\";
            openFileDialog1.Filter = "Excel files (*.xls)|*.xls|Excel x files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.FileName = "";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog1.FileName;

                // Open Excel workbook
                wkbook = excelapp.Workbooks.Open(fileName,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing);
            }
        }

        private void button_close_Click(object sender, EventArgs e)
        {
            String fileName;

            saveFileDialog1.InitialDirectory = "f:\\Climbing\\Stopwatch\\VC\\text_xls_sharp_v1\\";
            saveFileDialog1.Filter = "Excel files (*.xls)|*.xls|Excel x files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 3;
            saveFileDialog1.FileName = "test_new.xls";
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = saveFileDialog1.FileName;

                // Save Excel workbook
                wkbook.SaveAs(fileName,
                    Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                wkbook.Close(false, null, null);
            }
        }

        private void button_change_Click(object sender, EventArgs e)
        {
            Worksheet sheet = (Worksheet)wkbook.Sheets[1];
            Range cell_1;
            Range cell_2;
            Range cell_int;

            cell_int = sheet.get_Range("A5", "B5");

            cell_1          = sheet.get_Range("A1", "B1");
            cell_2          = sheet.get_Range("A4", "B4");

            cell_int.Value2 = cell_1.Value2;
            cell_1.Value2 = cell_2.Value2;
            cell_2.Value2 = cell_int.Value2;

            cell_1 = sheet.get_Range("A2", "B2");
            cell_2 = sheet.get_Range("A3", "B3");

            cell_int.Value2 = cell_1.Value2;
            cell_1.Value2 = cell_2.Value2;
            cell_2.Value2 = cell_int.Value2;

            cell_int.Value2 = "abba";


        }
    }
}
