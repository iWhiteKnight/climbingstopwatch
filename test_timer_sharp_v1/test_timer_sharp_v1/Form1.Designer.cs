﻿namespace test_timer_sharp_v1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_start = new System.Windows.Forms.Button();
            this.button_stop = new System.Windows.Forms.Button();
            this.label_t = new System.Windows.Forms.Label();
            this.label_d = new System.Windows.Forms.Label();
            this.label_time = new System.Windows.Forms.Label();
            this.label_duration = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(82, 51);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(185, 23);
            this.button_start.TabIndex = 0;
            this.button_start.Text = "Start";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.ClickStart);
            // 
            // button_stop
            // 
            this.button_stop.Location = new System.Drawing.Point(82, 99);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(185, 23);
            this.button_stop.TabIndex = 0;
            this.button_stop.Text = "Stop";
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.ClickStop);
            // 
            // label_t
            // 
            this.label_t.AutoSize = true;
            this.label_t.Location = new System.Drawing.Point(79, 160);
            this.label_t.Name = "label_t";
            this.label_t.Size = new System.Drawing.Size(33, 13);
            this.label_t.TabIndex = 1;
            this.label_t.Text = "Time:";
            // 
            // label_d
            // 
            this.label_d.AutoSize = true;
            this.label_d.Location = new System.Drawing.Point(79, 199);
            this.label_d.Name = "label_d";
            this.label_d.Size = new System.Drawing.Size(50, 13);
            this.label_d.TabIndex = 1;
            this.label_d.Text = "Duration:";
            // 
            // label_time
            // 
            this.label_time.Location = new System.Drawing.Point(131, 160);
            this.label_time.Name = "label_time";
            this.label_time.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_time.Size = new System.Drawing.Size(136, 13);
            this.label_time.TabIndex = 1;
            this.label_time.Text = "0";
            this.label_time.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_duration
            // 
            this.label_duration.Location = new System.Drawing.Point(131, 199);
            this.label_duration.Name = "label_duration";
            this.label_duration.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_duration.Size = new System.Drawing.Size(136, 13);
            this.label_duration.TabIndex = 1;
            this.label_duration.Text = "0";
            this.label_duration.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 340);
            this.Controls.Add(this.label_d);
            this.Controls.Add(this.label_duration);
            this.Controls.Add(this.label_time);
            this.Controls.Add(this.label_t);
            this.Controls.Add(this.button_stop);
            this.Controls.Add(this.button_start);
            this.Name = "Form1";
            this.Text = "Timer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.Label label_t;
        private System.Windows.Forms.Label label_d;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Label label_duration;
    }
}

