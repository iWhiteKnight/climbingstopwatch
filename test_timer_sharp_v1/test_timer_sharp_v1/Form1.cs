﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace test_timer_sharp_v1
{
    public partial class Form1 : Form
    {
        static uint time_i;
        Stopwatch stopWatch;
        Thread TimeThread = new Thread(TimeTask);
        

        public Form1()
        {
            InitializeComponent();
        }

        private void ClickStart(object sender, EventArgs e)
        {
            stopWatch = Stopwatch.StartNew();

            if (TimeThread.IsAlive)
                TimeThread.Abort();
            

            if (!TimeThread.IsAlive)
            {
                TimeThread = new Thread(TimeTask);
                TimeThread.IsBackground = true;
                TimeThread.Priority = ThreadPriority.Lowest;
                time_i = 0;
                TimeThread.Start(label_time);
            }
            else
                TimeThread.Abort();

            label_duration.Text = "Press Stop";
            
        }

        private void ClickStop(object sender, EventArgs e)
        {
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            string elapsedTime = String.Format("{0:00} s  {1:000} ms", ts.Seconds, ts.Milliseconds);
            label_duration.Text = elapsedTime;

            TimeThread.Abort();
        }

        private static void TimeTask(object lab_time)
        {
            Label l_time = (Label)lab_time;
            while (true)
            {
                l_time.Invoke(new MethodInvoker(delegate { l_time.Text = String.Format("{0:0}", time_i); }));
                Thread.Sleep(1000);
                    time_i++;
            }
        }

    }
}
