﻿namespace test_com_sharp_v1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.combo_port = new System.Windows.Forms.ComboBox();
            this.label_port = new System.Windows.Forms.Label();
            this.button_open = new System.Windows.Forms.Button();
            this.button_CTS = new System.Windows.Forms.Button();
            this.button_DSR = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // combo_port
            // 
            this.combo_port.FormattingEnabled = true;
            this.combo_port.Location = new System.Drawing.Point(103, 39);
            this.combo_port.Name = "combo_port";
            this.combo_port.Size = new System.Drawing.Size(121, 21);
            this.combo_port.TabIndex = 0;
            // 
            // label_port
            // 
            this.label_port.AutoSize = true;
            this.label_port.Location = new System.Drawing.Point(41, 47);
            this.label_port.Name = "label_port";
            this.label_port.Size = new System.Drawing.Size(29, 13);
            this.label_port.TabIndex = 1;
            this.label_port.Text = "Port:";
            // 
            // button_open
            // 
            this.button_open.Location = new System.Drawing.Point(103, 87);
            this.button_open.Name = "button_open";
            this.button_open.Size = new System.Drawing.Size(121, 23);
            this.button_open.TabIndex = 2;
            this.button_open.Text = "Open port";
            this.button_open.UseVisualStyleBackColor = true;
            this.button_open.Click += new System.EventHandler(this.button_open_Click);
            // 
            // button_CTS
            // 
            this.button_CTS.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.button_CTS.Enabled = false;
            this.button_CTS.Location = new System.Drawing.Point(28, 142);
            this.button_CTS.Name = "button_CTS";
            this.button_CTS.Size = new System.Drawing.Size(75, 69);
            this.button_CTS.TabIndex = 3;
            this.button_CTS.Text = "CTS/RTS \r\n(pin 8/7)";
            this.button_CTS.UseVisualStyleBackColor = false;
            // 
            // button_DSR
            // 
            this.button_DSR.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.button_DSR.Enabled = false;
            this.button_DSR.Location = new System.Drawing.Point(149, 142);
            this.button_DSR.Name = "button_DSR";
            this.button_DSR.Size = new System.Drawing.Size(75, 69);
            this.button_DSR.TabIndex = 3;
            this.button_DSR.Text = "DSR/DTR \r\n(pin 6/4)";
            this.button_DSR.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 240);
            this.Controls.Add(this.button_DSR);
            this.Controls.Add(this.button_CTS);
            this.Controls.Add(this.button_open);
            this.Controls.Add(this.label_port);
            this.Controls.Add(this.combo_port);
            this.Name = "Form1";
            this.Text = "COM";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox combo_port;
        private System.Windows.Forms.Label label_port;
        private System.Windows.Forms.Button button_open;
        private System.Windows.Forms.Button button_CTS;
        private System.Windows.Forms.Button button_DSR;
    }
}

