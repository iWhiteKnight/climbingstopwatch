﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;
using System.Threading;

namespace test_com_sharp_v1
{
    public partial class Form1 : Form
    {
        static SerialPort com_port;

        public Form1()
        {
            InitializeComponent();

            String[] com_port_names = SerialPort.GetPortNames();
            //String[] com_port_names = { "COM0", "COM1", "COM2" };
            if (com_port_names.Length == 0)
            {
                MessageBox.Show("No devices connected to COM-port");
                button_open.Enabled = false;
            }
            else
            {
                for (int i = 0; i < com_port_names.GetLength(0); i++)
                    combo_port.Items.Add(com_port_names[i]);
                combo_port.SelectedIndex = 0;
            }


        }

        private void button_open_Click(object sender, EventArgs e)
        {
            if (com_port == null)
            {
                combo_port.Enabled = false;
                com_port = new SerialPort(combo_port.Text);
                com_port.Handshake = Handshake.RequestToSendXOnXOff;
                com_port.DtrEnable = true;
                com_port.RtsEnable = true;
                com_port.Open();

                com_port.PinChanged += new SerialPinChangedEventHandler(com_event);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (com_port != null)
                if (com_port.IsOpen)
                    com_port.Close();
        }

        private void com_event(object sender, SerialPinChangedEventArgs event_args)
        {
            if (event_args.EventType == SerialPinChange.CtsChanged)
            {
                if (com_port.CtsHolding == true)
                    //MessageBox.Show("CTS = true");
                    button_CTS.BackColor = SystemColors.HotTrack;
                else
                    //MessageBox.Show("CTS = false");
                    button_CTS.BackColor = SystemColors.InactiveBorder;

            }
            else if (event_args.EventType == SerialPinChange.DsrChanged)
            {
                if (com_port.DsrHolding == true)
                    //MessageBox.Show("DSR = true");
                    button_DSR.BackColor = SystemColors.HotTrack;
                else
                    //MessageBox.Show("DSR = false");
                    button_DSR.BackColor = SystemColors.InactiveBorder;
            }
        }
    }
}
